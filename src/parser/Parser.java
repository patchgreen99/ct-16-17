package parser;

import ast.*;

import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;




/**
 * @author cdubach
 */
public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<Token>();

    private final Tokeniser tokeniser;



    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public Program parse() {
        // get the first token
        nextToken();

        return parseProgram();
    }

    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {

        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }
        System.out.println("Parsing error: expected ("+sb+") found ("+token+") at "+token.position);

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */
    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt=1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */
    private void nextToken() {
        //System.out.println(token);
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */
    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e == token.tokenClass) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }

        error(expected);
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */
    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }


    /*
    program    ::= (include)* (structdecl)* (vardecl)* (fundecl)* EOF
    */
    private Program parseProgram() {
        parseIncludes_kleene();
        List<StructType> sts = parseStructDecls_kleene();
        List<VarDecl> vds = parseVarDecls_kleene();
        List<FunDecl> fds = parseFunDecls_kleene();
        expect(TokenClass.EOF);
        return new Program(sts, vds, fds);
    }
    /*
        # #  comment
        # () grouping
        # [] optional
        # *  zero or more
        # +  one or more
        # |  alternative
    */


    /*
    include    ::= "#include" STRING_LITERAL
    */
    // includes are ignored, so does not need to return an AST node
    private void parseIncludes_kleene() {
	    if (accept(TokenClass.INCLUDE)) {
            nextToken();
            expect(TokenClass.STRING_LITERAL);
            parseIncludes_kleene();
        }

    }

    // to be completed ...


    /*
    structdecl ::= structtype "{" (vardecl)+ "}" ";"    # structure declaration
    */
    private List<StructType> parseStructDecls_kleene() {
        List<StructType> structDecls = new ArrayList<StructType>();

        // Handles case when struct intance is declared straight after
        while (accept(TokenClass.STRUCT) && lookAhead(2).tokenClass == TokenClass.LBRA) {
            expect(TokenClass.STRUCT);
            String name = value(expect(TokenClass.IDENTIFIER));
            expect(TokenClass.LBRA);

            VarDecl firstVarDecls = parseVarDecls();
            List<VarDecl> varDecls = new ArrayList<VarDecl>();
            varDecls.add(firstVarDecls);
            varDecls.addAll(parseVarDecls_kleene());

            expect(TokenClass.RBRA);
            expect(TokenClass.SC);

            structDecls.add(new StructType(name, varDecls));
        }
        return structDecls;
    }

    /*
    structtype ::= "struct" IDENT VarDecl*
    */
    private StructType parseStructType() {
        expect(TokenClass.STRUCT);
        List<VarDecl> varDecls = new ArrayList<VarDecl>();
        String refstruct = value(expect(TokenClass.IDENTIFIER));
        return new StructType(refstruct,varDecls);
    }


    /*
    vardecl    ::= type IDENT ";"                       # normal declaration, e.g. int a;
             | type IDENT "[" INT_LITERAL "]" ";"   # array declaration, e.g. int a[2];
    */
    private List<VarDecl> parseVarDecls_kleene() {
        List<VarDecl> varDecls = new ArrayList<VarDecl>();

        // Handle the only function matcher
        int i = 2;
        if (accept(TokenClass.STRUCT)){
            i += 1;
            if (lookAhead(2).tokenClass == TokenClass.ASTERIX){
                i+=1;
            }
        }
        if (lookAhead(1).tokenClass == TokenClass.ASTERIX){
            i+=1;
        }
        while (accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT) && lookAhead(i).tokenClass != TokenClass.LPAR){

            Type type = parseType();

            String name = value(expect(TokenClass.IDENTIFIER));

            if (accept(TokenClass.LSBR)){
                nextToken();

                Integer index = Integer.parseInt(value(expect(TokenClass.INT_LITERAL)));

                expect(TokenClass.RSBR);
                expect(TokenClass.SC);

                varDecls.add(new VarDecl(new ArrayType(type, index), name));

            } else {
                expect(TokenClass.SC);

                varDecls.add(new VarDecl(type, name));
            }

            // Handle the only function matcher
            i = 2;
            if (accept(TokenClass.STRUCT)){
                i += 1;
                if (lookAhead(2).tokenClass == TokenClass.ASTERIX){
                    i+=1;
                }
            }
            if (lookAhead(1).tokenClass == TokenClass.ASTERIX){
                i+=1;
            }
        }
        return varDecls;
    }



    private VarDecl parseVarDecls() {

        Type type = parseType();

        String name = value(expect(TokenClass.IDENTIFIER));

        if (accept(TokenClass.LSBR)){
            nextToken();

            Integer i = Integer.parseInt(value(expect(TokenClass.INT_LITERAL)));

            expect(TokenClass.RSBR);
            expect(TokenClass.SC);

            ArrayType at = new ArrayType(type, i);
            return new VarDecl(at, name);
        }

        expect(TokenClass.SC);
        return new VarDecl(type, name);
    }

    /*
    type       ::= ("int" | "char" | "void" | structtype) ["*"]
    */
    private Type parseType() {

        // Either a token or a structtype
        if (accept(TokenClass.STRUCT)){
            Type structtype = parseStructType();

            // Optional asterix
            if (accept(TokenClass.ASTERIX)){
                nextToken();
                return new PointerType(structtype);
            } else {
                return structtype;
            }

        } else {
            BaseType basetype = null;

            if (accept(TokenClass.CHAR)){
                nextToken();
                basetype = BaseType.CHAR;
            } else if (accept(TokenClass.INT)){
                nextToken();
                basetype = BaseType.INT;
            } else if (accept(TokenClass.VOID)){
                nextToken();
                basetype = BaseType.VOID;
            }

            // Optional asterix
            if (accept(TokenClass.ASTERIX)){
                nextToken();
                return new PointerType(basetype);
            } else {
                return basetype;
            }

        }
    }



    /*
    fundecl    ::= type IDENT "(" params ")" block    # function declaration
    */
    private List<FunDecl> parseFunDecls_kleene() {
        List<FunDecl> fundecls = new ArrayList<FunDecl>();

        // Handle the only function matcher
        int i = 2;
        if (accept(TokenClass.STRUCT)){
            i += 1;
            if (lookAhead(2).tokenClass == TokenClass.ASTERIX){
                i+=1;
            }
        }
        if (lookAhead(1).tokenClass == TokenClass.ASTERIX){
            i+=1;
        }

        // Includes the case for returning pointer too
        while (accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT) && lookAhead(i).tokenClass == TokenClass.LPAR){

            Type type = parseType();
            String name = value(expect(TokenClass.IDENTIFIER));
            expect(TokenClass.LPAR);
            List<VarDecl> varDecl = parseParams();
            expect(TokenClass.RPAR);
            Block block = parseBlock();

            fundecls.add(new FunDecl(type,name,varDecl,block));

            // Handle the only function matcher
            i = 2;
            if (accept(TokenClass.STRUCT)){
                i += 1;
                if (lookAhead(2).tokenClass == TokenClass.ASTERIX){
                    i+=1;
                }
            }
            if (lookAhead(1).tokenClass == TokenClass.ASTERIX){
                i+=1;
            }
        }

        return fundecls;
    }

    /*
    params     ::= [ type IDENT ("," type IDENT)* ]
    */
    private List<VarDecl> parseParams() {
        List<VarDecl> params = new ArrayList<VarDecl>();

        if (accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT)){
            Type type = parseType();
            String name = value(expect(TokenClass.IDENTIFIER));
            params.add(new VarDecl(type,name));

            while (accept(TokenClass.COMMA)){
                nextToken();
                params.add(new VarDecl(parseType(),value(expect(TokenClass.IDENTIFIER))));
            }
        }
        return params;
    }

    /*
    block      ::= "{" (vardecl)* (stmt)* "}"
    */
    private Block parseBlock() {
        expect(TokenClass.LBRA);
        List<VarDecl> vardecls = parseVarDecls_kleene();
        List<Stmt> stmts = parseStmt_kleene();
        expect(TokenClass.RBRA);
        return new Block(vardecls,stmts);
    }

    // ALL FINE ABOVE

    /*
    stmt       ::= block
             | "while" "(" exp ")" stmt              # while loop
             | "if" "(" exp ")" stmt ["else" stmt]   # if then else
            | "return" [exp] ";"                    # return


            // PARSE both start tokens as exp
            | lexp "=" exp ";"                      # assignment
             | exp ";"                               # expression statement, e.g. a function call
    */
    private List<Stmt> parseStmt_kleene() {
        List<Stmt> stmts = new ArrayList<Stmt>();

        // Please don't judge me
        while (accept(TokenClass.LBRA,TokenClass.WHILE,TokenClass.IF,TokenClass.RETURN,TokenClass.ASTERIX) || accept(TokenClass.IDENTIFIER) && lookAhead(1).tokenClass == TokenClass.ASSIGN || accept(TokenClass.IDENTIFIER, TokenClass.SIZEOF, TokenClass.LPAR, TokenClass.MINUS, TokenClass.INT_LITERAL, TokenClass.CHAR_LITERAL, TokenClass.STRING_LITERAL)) {
            if (accept(TokenClass.LBRA)) {

                Block block = parseBlock();

                stmts.add(block);

            } else if (accept(TokenClass.WHILE)) {

                nextToken();
                expect(TokenClass.LPAR);
                Expr expr = parseExp();
                expect(TokenClass.RPAR);
                Stmt stmt = parseStmt();

                stmts.add(new While(expr, stmt));

            } else if (accept(TokenClass.IF)) {

                nextToken();
                expect(TokenClass.LPAR);
                Expr expr = parseExp();
                expect(TokenClass.RPAR);
                Stmt stmt = parseStmt();

                if (accept(TokenClass.ELSE)) {
                    nextToken();
                    Stmt stmt2 = parseStmt();

                    stmts.add(new If(expr, stmt, stmt2));
                } else {

                    stmts.add(new If(expr, stmt));
                }

            } else if (accept(TokenClass.RETURN)) {

                nextToken();
                if (accept(TokenClass.SC)) {
                    nextToken();

                    stmts.add(new Return());
                } else {
                    Expr expr = parseExp();
                    expect(TokenClass.SC);

                    stmts.add(new Return(expr));
                }


            } else if (accept(TokenClass.ASTERIX)) {

                nextToken();
                Expr expr1 = parseExp();
                expect(TokenClass.ASSIGN);
                Expr expr2 = parseExp();
                expect(TokenClass.SC);

                stmts.add(new Assign(new ValueAtExpr(expr1), expr2));

            } else if (accept(TokenClass.IDENTIFIER) && lookAhead(1).tokenClass == TokenClass.ASSIGN) {

                Expr expr1 = new VarExpr(value(expect(TokenClass.IDENTIFIER)));
                nextToken();
                Expr expr2 = parseExp();
                expect(TokenClass.SC);

                stmts.add(new Assign(expr1, expr2));

            } else if (accept(TokenClass.IDENTIFIER, TokenClass.SIZEOF, TokenClass.LPAR, TokenClass.MINUS, TokenClass.INT_LITERAL, TokenClass.CHAR_LITERAL, TokenClass.STRING_LITERAL)) {

                Expr expr1 = parseExp();
                //  exp ";"
                if (accept(TokenClass.SC)) {
                    expect(TokenClass.SC);

                    stmts.add(new ExprStmt(expr1));

                    // exp "[" exp "]" "=" exp ";"
                    // exp "." IDENT "=" exp ";"

                } else if (accept(TokenClass.ASSIGN)) {
                    expect(TokenClass.ASSIGN);
                    Expr expr2 = parseExp();
                    expect(TokenClass.SC);

                    stmts.add(new Assign(expr1, expr2));
                } else {
                    error(TokenClass.SC);
                }

            }
        }

        return stmts;
    }


    private Stmt parseStmt(){
        if (accept(TokenClass.LBRA)) {
            return parseBlock();

        } else if (accept(TokenClass.WHILE)) {
            nextToken();
            expect(TokenClass.LPAR);
            Expr expr = parseExp();
            expect(TokenClass.RPAR);
            Stmt stmt = parseStmt();
            return new While(expr,stmt);

        } else if (accept(TokenClass.IF)) {
            nextToken();
            expect(TokenClass.LPAR);
            Expr expr = parseExp();
            expect(TokenClass.RPAR);
            Stmt stmt1 = parseStmt();

            if (accept(TokenClass.ELSE)) {
                nextToken();
                Stmt stmt2 = parseStmt();
                return new If(expr,stmt1,stmt2);
            } else {
                return new If(expr,stmt1);
            }

        } else if (accept(TokenClass.RETURN)) {
            nextToken();
            if (accept(TokenClass.SC)) {
                nextToken();
                return new Return();
            } else {
                Expr expr = parseExp();
                expect(TokenClass.SC);
                return new Return(expr);
            }


        } else if (accept(TokenClass.ASTERIX)) {
            nextToken();
            Expr expr1 = parseExp();
            expect(TokenClass.ASSIGN);
            Expr expr2 = parseExp();
            expect(TokenClass.SC);
            return new Assign(expr1,expr2);

        } else if (accept(TokenClass.IDENTIFIER) && lookAhead(1).tokenClass == TokenClass.ASSIGN) {
            VarExpr expr1 = new VarExpr(value(expect(TokenClass.IDENTIFIER)));
            nextToken();
            Expr expr2 = parseExp();
            expect(TokenClass.SC);
            return new Assign(expr1,expr2);


        } else if (accept(TokenClass.IDENTIFIER,TokenClass.SIZEOF,TokenClass.LPAR,TokenClass.MINUS,TokenClass.INT_LITERAL,TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL)) {
            Expr expr1 = parseExp();

            //  exp ";"
            if (accept(TokenClass.SC)) {
                nextToken();

                return new ExprStmt(expr1);

                // exp "[" exp "]" "=" exp ";"
                // exp "." IDENT "=" exp ";"

            } else if (accept(TokenClass.ASSIGN)) {
                nextToken();
                Expr expr2  = parseExp();
                expect(TokenClass.SC);
                return new Assign(expr1,expr2);

            } else {
                error(TokenClass.SC);
            }

        } else {
            error();
        }

        return null;

    }




    // parseAndTerms - > parseExpBottem

    //exp = LV1

    private Expr parseExp(){
        return parseOrTerms();
    }

    // LV1 = LV2 LV1'

    private Expr parseOrTerms(){
        Expr expr1 = parseAndTerms();
        return parseOr(expr1);
    }

    // LV1' = ("||") LV2 LV1' | epsilon

    private Expr parseOr(Expr expr1){
        if (accept(TokenClass.OR)){
            nextToken();
            Op op = Op.OR;
            Expr expr2 = parseAndTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseOr(nestedbinop);
        }
        return expr1;
    }




    // LV2 = LV3 LV2'

    private Expr parseAndTerms(){
        Expr expr1 = parseComparisonTerms();
        return parseAnd(expr1);
    }

    // LV2' = ("&&") LV3 LV2' | epsilon

    private Expr parseAnd(Expr expr1){
        if (accept(TokenClass.AND)){
            nextToken();
            Op op = Op.AND;
            Expr expr2 = parseComparisonTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseAnd(nestedbinop);
        }
        return expr1;
    }

    // LV3 = LV4 LV3'

    private Expr parseComparisonTerms(){
        Expr expr1 = parseGreaterTerms();
        return parseComparisons(expr1);
    }

    // LV3' = ("=="|"!=") LV4 LV3' | epsilon

    private Expr parseComparisons(Expr expr1){
            if (accept(TokenClass.EQ)){
                nextToken();
                Op op = Op.EQ;
                Expr expr2 = parseGreaterTerms();
                Expr nestedbinop = new BinOp(expr1,op,expr2);
                return parseComparisons(nestedbinop);

            } else if (accept(TokenClass.NE)){
                nextToken();
                Op op = Op.NE;
                Expr expr2 = parseGreaterTerms();
                Expr nestedbinop = new BinOp(expr1,op,expr2);
                return parseComparisons(nestedbinop);
            }

        return expr1;
    }

    // LV4 = LV5 LV4'

    private Expr parseGreaterTerms(){
        Expr expr1 = parseAdditionTerms();
        return parseGreater(expr1);
    }

    // LV4' = ("<"|">"|"<="|">=") LV5 LV4' | epsilon

    private Expr parseGreater(Expr expr1){
        if (accept(TokenClass.GE)){
            Op op = Op.GE;
            nextToken();
            Expr expr2 = parseAdditionTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseGreater(nestedbinop);
        } else if (accept(TokenClass.LE)){
            Op op = Op.LE;
            nextToken();
            Expr expr2 = parseAdditionTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseGreater(nestedbinop);
        } else if (accept(TokenClass.LT)) {
            Op op = Op.LT;
            nextToken();
            Expr expr2 = parseAdditionTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseGreater(nestedbinop);
        } else if (accept(TokenClass.GT)) {
            Op op = Op.GT;
            nextToken();
            Expr expr2 = parseAdditionTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseGreater(nestedbinop);
        }
        return expr1;
    }

    // LV5 = LV6 LV5'

    private Expr parseAdditionTerms(){
        Expr expr1 = parseMultiplicationTerms();
        return parseAddition(expr1);
    }

    // LV5' = ("+" | "-") LV6 LV5' | epsilon

    private Expr parseAddition(Expr expr1){
        if (accept(TokenClass.PLUS)){
            Op op = Op.ADD;
            nextToken();
            Expr expr2 = parseMultiplicationTerms();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseAddition(nestedbinop);
        } else if (accept(TokenClass.MINUS)){
            Op op = Op.SUB;
            nextToken();
            Expr expr2 = parseMultiplicationTerms();
            Expr nestedbinop =  new BinOp(expr1,op,expr2);
            return parseAddition(nestedbinop);
        }
        return expr1;
    }

    // LV6 = LV7 LV6'

    private Expr parseMultiplicationTerms(){
        Expr expr1 = parseSizeof();
        return parseMultiplication(expr1);
    }

    // LV6' = ("*" || "/" || "%") LV7 LV6' | epsilon

    private Expr parseMultiplication(Expr expr1){
        if (accept(TokenClass.ASTERIX)){
            Op op = Op.MUL;
            nextToken();
            Expr expr2 = parseSizeof();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseMultiplication(nestedbinop);
        } else if (accept(TokenClass.DIV)){
            Op op = Op.DIV;
            nextToken();
            Expr expr2 = parseSizeof();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseMultiplication(nestedbinop);
        } else if (accept(TokenClass.REM)){
            Op op = Op.MOD;
            nextToken();
            Expr expr2 = parseSizeof();
            Expr nestedbinop = new BinOp(expr1,op,expr2);
            return parseMultiplication(nestedbinop);
        }
        return expr1;
    }



    // CHECK EVERYTHING BELLOW HERE !!!!!!!!!!!!!!!!!!




    // LV7 = LV7' LV8 | "sizeof" "(" type ")"

    private Expr parseSizeof(){
        if (accept(TokenClass.SIZEOF)){
            nextToken();
            expect(TokenClass.LPAR);
            Type type = parseType();
            expect(TokenClass.RPAR);
            return new SizeOfExpr(type);
        } else {
            return parseTypeCast();
        }
    }

    // LV7' =  "*" LV7' | "(" type ")" LV7' | epsilon

    private Expr parseTypeCast(){
        if (accept(TokenClass.ASTERIX)){
            nextToken();
            Expr expr = parseTypeCast();
            return new ValueAtExpr(expr);
        } else if (accept(TokenClass.LPAR) && (lookAhead(1).tokenClass == TokenClass.INT || lookAhead(1).tokenClass == TokenClass.CHAR || lookAhead(1).tokenClass == TokenClass.VOID || lookAhead(1).tokenClass == TokenClass.STRUCT )) {
            nextToken();
            Type type = parseType();
            expect(TokenClass.RPAR);
            Expr expr = parseTypeCast();
            return new TypecastExpr(expr, type);
        }
        return parseFieldAccessTerms();
    }

    // LV7.5 = LV7.5'

    private Expr parseFieldAccessTerms(){
        //Expr expr1 = parseArrayTerms();
        //return parseFieldAccess(expr1);

        // JUMPED THE FIELD ACCESS AND ADDED IT TO THE ARRAY ACCESS ONE


        return parseArrayTerms();
    }








    // LV7.5' = LV8 ("." IDENT)*

    //private Expr parseFieldAccess(Expr expr1){

    //    if (accept(TokenClass.DOT)){
    //        nextToken();
    //        String s = value(expect(TokenClass.IDENTIFIER));
    //        return new FieldAccessExpr(parseFieldAccess(expr1),s);
    //    }
    //    return expr1;
    //}



    //LV8 = LV9 LV8'

    private Expr parseArrayTerms(){
        Expr expr1 = parseExpBottom();
        return parseArray(expr1);
    }

    // LV8' =  (arraycall)* | epsilon

    private Expr parseArray(Expr expr1){ // LV9 is either 2 Tokens or 1
        if (accept(TokenClass.LSBR)) {
            Expr frontexpr = parseArrayCall(expr1);
            return parseArray(frontexpr);
        } else if (accept(TokenClass.DOT)){
            nextToken();
            String s = value(expect(TokenClass.IDENTIFIER));
            return new FieldAccessExpr(parseArray(expr1),s);
        }
        return expr1;
        //return expr1;
    }

    private Expr parseArrayCall(Expr expr1){
        if (accept(TokenClass.LSBR)) {
            Expr arrayindex = parseSquare();
            expect(TokenClass.RSBR);
            return new ArrayAccessExpr(expr1,arrayindex);
        }
        return expr1;
    }

    // square = "[" exp

    private Expr parseSquare(){
        expect(TokenClass.LSBR);
        return parseExp();
    }


    // round = "(" exp

    private Expr parseRound(){
        expect(TokenClass.LPAR);
        return parseExp();
    }


    // LV10 = ["-"] (IDENT | INT_LITERAL) | CHAR_LITERAL | STRING_LITERAL  |

    private Expr parseExpBottom(){
        if (accept(TokenClass.CHAR_LITERAL)) {
            return new CharLiteral(value(expect(TokenClass.CHAR_LITERAL)).charAt(0));
        } else if (accept(TokenClass.STRING_LITERAL)) {
            return new StrLiteral(value(expect(TokenClass.STRING_LITERAL)));
        } else if (accept(TokenClass.INT_LITERAL)) {
            return new IntLiteral(Integer.parseInt(value(expect(TokenClass.INT_LITERAL))));

        } else if (accept(TokenClass.MINUS) && lookAhead(1).tokenClass == TokenClass.INT_LITERAL) {
            nextToken();
            Integer i = Integer.parseInt(value(expect(TokenClass.INT_LITERAL)));
            return new BinOp(new IntLiteral(0),Op.SUB,new IntLiteral(i));

        } else if (accept(TokenClass.MINUS) && lookAhead(1).tokenClass == TokenClass.IDENTIFIER){
            nextToken();
            Expr expr = new VarExpr(value(expect(TokenClass.IDENTIFIER)));
            return new BinOp(new IntLiteral(0),Op.SUB,expr);

            // "(" exp ")"

        } else if (accept(TokenClass.LPAR)) {
            Expr expr = parseRound();
            expect(TokenClass.RPAR);
            return expr;

            // FUNCALL LV0' = IDENT "(" [ LV0' LV1 ("," LV0' LV1)* ] ")" | epsilon

        } else if (accept(TokenClass.IDENTIFIER) && lookAhead(1).tokenClass == TokenClass.LPAR) {
            String name = value(expect(TokenClass.IDENTIFIER));
            nextToken();
            List<Expr> params = new ArrayList<Expr>();
            if (accept(TokenClass.IDENTIFIER, TokenClass.SIZEOF, TokenClass.LPAR, TokenClass.MINUS, TokenClass.INT_LITERAL, TokenClass.CHAR_LITERAL, TokenClass.STRING_LITERAL, TokenClass.ASTERIX)) {
                Expr expr1 = parseExp();
                params.add(expr1);
                while (accept(TokenClass.COMMA)) {
                    nextToken();
                    Expr expr2 = parseExp();
                    params.add(expr2);
                }
            }
            expect(TokenClass.RPAR);
            return new FunCallExpr(name, params);

        } else if (accept(TokenClass.IDENTIFIER)){
            return new VarExpr(value(expect(TokenClass.IDENTIFIER)));

        } else {
            error();
        }
        return null;
    }

    private String value(Token t){
        if (t != null){
            return t.data;
        } else {
            return null;
        }
    }


}
    
