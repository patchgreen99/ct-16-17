package sem;

import ast.VarDecl;

/**
 * Created by patrick.green on 23/10/2016.
 */
public class VarSymbol extends Symbol {
    public VarDecl vardecl;
    public String name;

    public VarSymbol(VarDecl var){
        this.vardecl = var;
        this.name = var.varName;
    }

}
