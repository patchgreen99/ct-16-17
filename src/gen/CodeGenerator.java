package gen;

import ast.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.List;

public class CodeGenerator implements ASTVisitor<Register> {

    /*
        * Simple register allocator.
            */



    // contains all the free temporary registers
    private Stack<Register> freeRegs = new Stack<Register>();
    public Register var2assign;

    public CodeGenerator() {
        this.genscope = new GenScope() ;
        freeRegs.addAll(Register.tmpRegs);

        // assigning register
        this.var2assign = getRegister();

    }

    private class RegisterAllocationError extends Error {}

    private Register getRegister() {
        try {
            return freeRegs.pop();
        } catch (EmptyStackException ese) {
            throw new RegisterAllocationError(); // no more free registers, bad luck!
        }
    }

    private void freeRegister(Register reg) {
        freeRegs.push(reg);
    }




    // Remember register on declaration

    public List<VarDecl> funcparams = new ArrayList<VarDecl>();
    public GenScope genscope;
    public List<Set<Register>> stack_of_temps = new ArrayList<Set<Register>>();

    public PrintWriter writer; // use this writer to output the assembly instructions
    public Integer whilecounter = 0;
    public Integer ifcounter = 0;
    public Integer boolcounter = 0;
    public Integer stringcounter = 0;


    public void emitProgram(Program program, File outputFile) throws FileNotFoundException {
        writer = new PrintWriter(outputFile);
        // Handles memory alocation grlobal and stack
        MemoryAllocation mem = new MemoryAllocation();
        mem.visitProgram(program);

        visitProgram(program);
        writer.close();
    }

    @Override
    public Register visitBaseType(BaseType bt) {
        writer.println("#basetype");

        writer.println("#");
        return null;

    }

    @Override
    public Register visitStructType(StructType st) {
        writer.println("#structtype");
        if (st.params.size() > 0) {
            Register input = getRegister();
            writer.println("la " + input + ", " + "INT");
            genscope.putglobal(false, st.structName, writer, input); // head of struct

            for (int i = 0; i < st.params.size(); i++) {
                if (st.params.get(i).type == BaseType.INT) {
                    genscope.gp_offset -= 4;
                    writer.println("la " + input + ", " + "INT");
                    writer.println("sw " + input + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                } else if (st.params.get(i).type == BaseType.CHAR) {
                    genscope.gp_offset -= 4;
                    writer.println("la " + input + ", " + "CHAR");
                    writer.println("sw " + input + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                } else if (st.params.get(i).type instanceof ArrayType) {
                    ArrayType array = ((ArrayType) st.params.get(i).type);
                    writer.println("la " + input + ", " + array.type);
                    genscope.gp_offset -= 4;
                    writer.println("sw " + input + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                    int length = array.IntLiteral;
                    writer.println("subu " + Register.sp + ", " + Register.sp + ", " + 4 * (length - 1));

                    // Loop over to fill the rest
                    int counter = 0;
                    writer.println("la " + input + ", " + array.type);
                    while (counter < length) {
                        genscope.fp_offset -= 4;
                        writer.println("sw " + input + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                        counter += 1;
                    }
                } else if (st.params.get(i).type instanceof PointerType) {
                    PointerType pointer = ((PointerType) st.params.get(i).type);
                    writer.println("la " + input + ", " + pointer.type);
                    genscope.gp_offset -= 4;
                    writer.println("sw " + input + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                }
            }

            freeRegister(input);
        }


        writer.println("#");
        return null;
    }

    @Override
    public Register visitBlock(Block b) {
        writer.println("#block");
        // TODO: to complete

        GenScope oldgenscope = genscope ;
        genscope = new GenScope(genscope);


        // look above your frame pointer for your new parameters
        int stackpos = 0;
        int paramcounter = funcparams.size();
        while (paramcounter > 0){ // must go in reverse order
            paramcounter -= 1;
            stackpos += 4;
            Register temp = getRegister();
            writer.println("lw " + temp + ", " + stackpos + "(" + Register.fp + ")");
            genscope.put(false,funcparams.get(paramcounter).varName,writer,temp);
            freeRegister(temp);
        }

        // Clear all function parameters
        funcparams = new ArrayList<VarDecl>();

        // nothing to do until variable involved
        for (VarDecl vd : b.vardecls){
            vd.accept(this);
        }

        for (Stmt st: b.stmts){
            st.accept(this);
        }

        // End of function
        genscope = oldgenscope;

        writer.println("#");
        return null;
    }

    @Override
    public Register visitFunDecl(FunDecl p) {
        writer.println("#fundecl");
        // TODO: to complete

        writer.println();
        writer.println(p.name + ":");


        // Parameters of method, save them all to functparams
        for (VarDecl vd : p.params) {

            // functparams must be called inside the new scope
            // funcparams are saved to an additional HashMap
            funcparams.add(vd);
        }

        // deal with fp and ra
        mips_precall();

        // Deal with local/temps space assignement
        writer.println("subu " + Register.sp + ", " + Register.sp + ", " + p.incrementstack*4);

        p.block.accept(this);

        // deal with fp and ra
        mips_postreturn();

        // Pop parameters of the stack
        writer.println("add " + Register.sp + ", " + Register.sp + ", " + p.params.size() * 4);

        writer.println("#");
        return null;
    }

    @Override
    public Register visitProgram(Program p) {
        writer.println("#program");
        // TODO: to complete
        writer.println(".data");
        //TODO : structs still to do



        for (StructType st :p.structTypes){
            st.accept(this);
        }
//
//        // all struct instances
//        for (VarDecl vd : p.structinstances){
//            for (VarDecl param : vd.structdecl.params){
//                writer.println(vd.varName + "_" + vd.structdecl.structName + "_" + param.varName + ": .word");
//            }
//        }

        // all array accesses
        for (VarDecl vd : p.arrayaccess){
            writer.println(vd.varName + ": .word   0 : " + ((ArrayType) vd.type).IntLiteral);
        }

        // all strings
        Integer tempcounter = 0;
        for(String s: p.strings){
            writer.println("str"+tempcounter + ": .asciiz \"" + s + "\"");
            tempcounter += 1;
        }

        writer.println("INT: .word");
        writer.println("CHAR: .word");

        writer.println(".text");

        // All global variables

        // TODO: ARRAY GLOBALLY
        for (VarDecl vd : p.varDecls) {
            Register temp = getRegister();
            if (vd.type instanceof ArrayType) {
                writer.println("la " + temp + ", " + ((ArrayType) vd.type).type);
                genscope.putglobal(false,vd.varName, writer, temp);
                int length = ((ArrayType) vd.type).IntLiteral;

                // Loop over to fill the rest
                int counter = 1;
                writer.println("la " + temp + ", " + ((ArrayType) vd.type).type);
                while (counter < length) {
                    genscope.gp_offset -= 4;
                    writer.println("sw " + temp + ", " + genscope.gp_offset + "(" + Register.gp + ")");
                    counter += 1;
                }

            } else if (vd.type instanceof PointerType) {
                PointerType pointer = ((PointerType) vd.type);
                writer.println("la " + temp + ", " + pointer.type);
                genscope.putglobal(true, vd.varName, writer, temp);

            } else if (vd.type instanceof BaseType) {
                writer.println("la " + temp + ", " + vd.type);
                genscope.putglobal(false,vd.varName, writer, temp);
            } else if (vd.type instanceof StructType){
                Offset o = genscope.lookup(((StructType) vd.type).structName);
                genscope.symbolTable.put(vd.varName, new Offset(o.offset,true,false));
            }
            freeRegister(temp);
        }

        // Set the return address of gloabl to end
        writer.println("jal main"); // always run main

        // Terminate program
        writer.println("end: ");
        writer.println("li" + " " + Register.v0 + ", " + "10");
        writer.println("syscall");



        for (FunDecl fd :p.funDecls){
            fd.accept(this);
        }


        writer.println("");
        writer.println("#Permenent functions");
        writer.println("");


        writer.println("print_s:");
        writer.println("li" + " " + Register.v0 + ", " + "4");
        writer.println("syscall");
        writer.println("jr " + Register.ra);


        writer.println("");
        writer.println("print_i:");
        // deal with fp and ra
        mips_precall();
        writer.println("lw " + Register.paramRegs[0] + ", " + 4 + "(" + Register.fp + ")");
        writer.println("li" + " " + Register.v0 + ", " + "1");
        writer.println("syscall");
        // deal with fp and ra
        mips_postreturn();
        // Pop parameters of the stack
        writer.println("add " + Register.sp + ", " + Register.sp + ", " + 4);

        writer.println("");
        writer.println("print_c:");
        // deal with fp and ra
        mips_precall();
        writer.println("lw " + Register.paramRegs[0] + ", " + 4 + "(" + Register.fp + ")");
        writer.println("li" + " " + Register.v0 + ", " + "11");
        writer.println("syscall");
        // deal with fp and ra
        mips_postreturn();
        // Pop parameters of the stack
        writer.println("add " + Register.sp + ", " + Register.sp + ", " + 4);

        writer.println("");
        writer.println("read_c:");
        // deal with fp and ra
        mips_precall();
        writer.println("li" + " " + Register.v0 + ", " + "12");
        writer.println("syscall");
        // deal with fp and ra
        mips_postreturn();

        writer.println("");
        writer.println("read_i:");
        // deal with fp and ra
        mips_precall();
        writer.println("li" + " " + Register.v0 + ", " + "5");
        writer.println("syscall");
        // deal with fp and ra
        mips_postreturn();

        writer.println("#");
        return null;
    }

    @Override
    public Register visitVarDecl(VarDecl vd) {
        writer.println("#vardecl");

        // TODO: ARRAY LOCALLY
        if (vd.type == BaseType.INT){
            genscope.put(false,vd.varName, writer, null);
        } else if (vd.type == BaseType.CHAR) {
            genscope.put(false,vd.varName, writer, null);
        } else if (vd.type instanceof ArrayType) {
            Register temp = getRegister();
            ArrayType array = ((ArrayType) vd.type);
            writer.println("la " + temp + ", " + array.type);
            genscope.put(false,vd.varName, writer, temp);
            int length = array.IntLiteral;
            writer.println("subu " + Register.sp + ", " + Register.sp + ", " + 4 * (length - 1));

            // Loop over to fill the rest
            int counter = 1;
            writer.println("la " + temp + ", " + array.type);
            while (counter < length) {
                genscope.fp_offset -= 4;
                writer.println("sw " + temp + ", " + genscope.fp_offset + "(" + Register.fp + ")");
                counter += 1;
            }
            freeRegister(temp);
        } else if (vd.type instanceof PointerType){
            Register temp = getRegister();
            PointerType pointer = ((PointerType) vd.type);
            writer.println("la " + temp + ", " + pointer.type);
            genscope.put(true, vd.varName, writer, temp);
            freeRegister(temp);
        } else if (vd.type instanceof StructType) {
            Offset o = genscope.lookup(((StructType) vd.type).structName);
            genscope.symbolTable.put(vd.varName, new Offset(o.offset,false,false));
        }

        writer.println("#");
        return null;
    }

    @Override
    public Register visitVarExpr(VarExpr v) {
        writer.println("#varexpr");
        // TODO: to complete

        Register addrReg = getRegister();
        Register result = getRegister();
        Offset o = genscope.lookup(v.name);

        if (o.isglobal){
            writer.println("la" + ", " + addrReg + ", " + genscope.lookup(v.name).offset + "(" + Register.gp + ")");
        } else {
            writer.println("la" + ", " + addrReg + ", " + genscope.lookup(v.name).offset + "(" + Register.fp + ")");
        }


        writer.println("#");
        freeRegister(addrReg);

        // Handles pointer assignment
        if (o.ispointer) {
            // for assigning
            writer.println("la " + var2assign + ", " + "0(" + addrReg + ")");

            return var2assign;
        } else {
            // for assigning
            writer.println("la " + var2assign + ", " + "0(" + addrReg + ")");

            // for returning
            writer.println("lw " + result + ", " + "0(" + addrReg + ")");

            return result;
        }
    }

    @Override
    public Register visitArrayType(ArrayType at){
        writer.println("#arraytype");
        // TODO: to complete

        writer.println("#");
        return null;
    }
    @Override
    public Register visitFunCallExpr(FunCallExpr fce){
        writer.println("#funcallexpr");
        // TODO: to complete

        // Jump to method
        switch(fce.name){
            case "print_s":
                Register arg = fce.params.get(0).accept(this);
                writer.println("move " + Register.paramRegs[0] + ", " + arg);
                writer.println("jal " + fce.name); // all other functions
                freeRegister(arg);
                break;

            default:
                // Push old registers onto stack
                savetemp();

                // Push parameters onto the stack
                for (Expr e: fce.params) {
                    Register paramreg = e.accept(this);
                    writer.println("subu " + Register.sp + ", " + Register.sp + ", " + "4"); // decrement sp to make space for param
                    writer.println("sw " + paramreg + ", " + "4(" + Register.sp + ")"); // copy param to the stack
                    freeRegister(paramreg);
                }

                writer.println("jal " + fce.name); // all other functions

                // Save old reg of stack
                readtemp();

                break;

            //case "mcmalloc":

        }

        writer.println("#");

        Register returnreg = getRegister();
        writer.println("move " + returnreg + ", " + Register.v0);
        return returnreg;
    }
    @Override
    public Register visitOp(Op v){
        writer.println("#op");
        // TODO: to complete

        writer.println("#");
        return null;
    }
    @Override
    public Register visitBinOp(BinOp bo){
        writer.println("#binop");
        // TODO: to complete

        Register lhsReg = bo.expr1.accept(this);
        Register rhsReg = bo.expr2.accept(this);
        Register result = getRegister();
        switch(bo.op) {
            case ADD: writer.println("add " + result + ", " + lhsReg + ", " + rhsReg);
                break;
            case MUL: writer.println("mul " + result + ", " + lhsReg + ", " + rhsReg);
                break;
            case SUB: writer.println("sub " + result + ", " + lhsReg + ", " + rhsReg);
                break;
            case AND: writer.println("and " + result + ", " + lhsReg + ", " + rhsReg);
                break;
            case OR: writer.println("or " + result + ", " + lhsReg + ", " + rhsReg);
                break;
            case EQ: writer.println("beq " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;
            case NE: writer.println("bne " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;
            case LE: writer.println("ble " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;
            case LT: writer.println("blt " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;
            case GT: writer.println("bgt " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;
            case GE: writer.println("bge " + lhsReg + ", " + rhsReg + ", " + "booltrue"+boolcounter);
                mips_boolean(result);
                break;

            case MOD:
                writer.println("div " + lhsReg + ", " + rhsReg);
                writer.println("mfhi" + " " + result);
                break;
            case DIV:
                writer.println("div " + lhsReg + ", " + rhsReg);
                writer.println("mflo" + " " + result);
                break;
        }

        freeRegister (lhsReg);
        freeRegister (rhsReg);

        writer.println("#");
        return result;
    }
    @Override
    public Register visitAssign(Assign a){
        writer.println("#assign");
        // TODO: to complete

        // TODO: ARRAY ASSIGNING
        // do right hand side first correct var2assign is saved
        Register rhs = a.expr2.accept(this);
        Register lhs = a.expr1.accept(this);

        writer.println("sw " + rhs + ", " + "0(" + var2assign + ")");

        freeRegister(lhs);
        freeRegister(rhs);

        writer.println("#");
        return null;
    }
    @Override
    public Register visitArrayAccessExpr(ArrayAccessExpr aae){
        writer.println("#arrayaccessexpr");
        // TODO: to complete

        Register addrReg = getRegister();
        Register result = getRegister();
        Register index = aae.expr2.accept(this);
        Register temp = getRegister();


        // TODO: MUST COMPLETE ARRAY
        // pull the left most var2assign out
        aae.expr1.accept(this);

//        Offset o = genscope.lookup(var2assign);
//        if (o.isglobal) {
//            writer.println("la " + addrReg + ", " + o.offset + "(" + Register.gp + ")");
//        } else {
//            writer.println("la " + addrReg + ", " + o.offset + "(" + Register.fp + ")");
//        }
        writer.println("add " + index + ", " + index + ", " + index);
        writer.println("add " + index + ", " + index + ", " + index);
        writer.println("add " + temp + ", " + var2assign + ", " + index);

        // for assigning
        writer.println("la " + var2assign + ", " + "0(" + temp + ")");

        // for returning
        writer.println("lw " + result + ", " + "0(" + temp + ")");

        writer.println("#");
        freeRegister(addrReg);
        freeRegister(temp);
        freeRegister(index);

        return result;
    }
    @Override
    public Register visitCharLiteral(CharLiteral cl){
        writer.println("#charliteral");
        // TODO: to complete
        Register result = getRegister();
        writer.println("li" + " " + result + ", " + "'" + cl.CharLiteral + "'");

        writer.println("#");
        return result;
    }
    @Override
    public Register visitExprStmt(ExprStmt es){
        writer.println("#exprstmt");
        // TODO: to complete
        Register reg = es.expr.accept(this);
        freeRegister(reg);
        writer.println("#");
        return null;
    }
    @Override
    public Register visitFieldAccessExpr(FieldAccessExpr fae){
        writer.println("#fieldaccessexpr");

        // Pull out left most varexp
        fae.expr.accept(this);
        Register result = getRegister();

        // TODO: YOU CAN FIND YOUR STRUCT OFFSET USING THIS
        //System.out.println(fae.structdeclaration);

        // TODO STRUCTS HERE ARE PRINTED IN ORDER
        int index = -4;
        for (VarDecl p : fae.structdeclaration.params){
            index -= 4;
            System.out.println(p.varName);
            if (p.varName.equals(fae.field)){
                writer.println("addi " + var2assign + ", " + var2assign + ", " + index);
                break;
            }

        }

        // for assigning not required
        //writer.println("la " + result + ", " + "0(" + var2assign + ")");

        // for returning
        writer.println("lw " + result + ", " + "0(" + var2assign + ")");

        writer.println("#");
        return result;
    }
    @Override
    public Register visitIf(If i){
        writer.println("#if");
        // TODO: to complete
        Register boolreg = i.bool.accept(this);

        int tempcounter = ifcounter;
        ifcounter += 1;

        writer.println("beqz" + " " + boolreg + ", " + "iffalse" + tempcounter);
        i.content1.accept(this);
        writer.println("b " + "iftrue" + tempcounter);
        writer.println("iffalse" + tempcounter + " :");
        if (! (i.content2 == null)){
            i.content2.accept(this);
        }
        writer.println("iftrue" + tempcounter + " :");

        writer.println("#");
        freeRegister(boolreg);
        return null;
    }
    @Override
    public Register visitIntLiteral(IntLiteral i){
        writer.println("#intliteral");
        // TODO: to complete
        Register result = getRegister();
        writer.println("li" + " " + result + ", " + i.IntLiteral);

        writer.println("#");
        return result;
    }
    @Override
    public Register visitPointerType(PointerType pt){
        writer.println("#pointertype");
        // TODO: to complete

        writer.println("#");
        return null;
    }
    @Override
    public Register visitReturn(Return r){
        writer.println("#return");
        // TODO: to complete
        Register returnreg = r.expr.accept(this);
        writer.println("move " + Register.v0 + ", " + returnreg); //assign return value into $v0

        // deal with fp and ra
        mips_postreturn();

        // Pop parameters of the stack
        writer.println("add " + Register.sp + ", " + Register.sp + ", " + funcparams.size()*4);

        // Clear all function parameters
        funcparams = new ArrayList<VarDecl>();

        writer.println("#");
        freeRegister(returnreg);
        return null;
    }
    @Override
    public Register visitSizeOfExpr(SizeOfExpr soe){
        writer.println("#sizeof");
        // TODO: to complete
        Register ret = getRegister();
        if (soe.type == BaseType.CHAR) {
            writer.println("li " + ret + ", " + "1");
        } else if (soe.type == BaseType.INT) {
            writer.println("li " + ret + ", " + "4");
        } else if (soe.type instanceof PointerType) {
            writer.println("li " + ret + ", "+ "4");
        }

        writer.println("#");
        return ret;
    }
    @Override
    public Register visitStrLiteral(StrLiteral s){
        writer.println("#strliteral");
        // TODO: to complete
        Register result = getRegister();
        writer.println("la" + " " + result + ", " + "str" + stringcounter);
        stringcounter += 1;
        writer.println("#");
        return result;
    }
    @Override
    public Register visitTypecastExpr(TypecastExpr tce){
        writer.println("#typecastexpr");
        // TODO: to complete

        writer.println("#");
        return tce.expr.accept(this);
    }
    @Override
    public Register visitValueAtExpr(ValueAtExpr vae){
        writer.println("#valueatexpr");
        // TODO: to complete

        Register result = getRegister();
        Register temp = vae.expr.accept(this);

        // for assigning
        writer.println("la " + var2assign + ", " + "0(" + temp + ")");

        // for returning
        writer.println("lw " + result + ", " + "0(" + temp + ")");

        writer.println("#");
        freeRegister(temp);

        return result;
    }
    @Override
    public Register visitWhile(While w){
        writer.println("#while");
        // TODO: to complete
        int tempcounter = whilecounter;
        whilecounter += 1;
        writer.println("while" + tempcounter + ":"); // must go above

        Register boolreg = w.bool.accept(this);

        writer.println("beqz" + ", " + boolreg + ", " + "whilefalse" + tempcounter);
        w.content.accept(this);
        writer.println("b " + "while" + tempcounter);
        writer.println("whilefalse" + tempcounter + ":");

        writer.println("#");
        freeRegister(boolreg);
        return null;
    }





    // Helper printers

    public void mips_boolean(Register result){
        writer.println("li" + ", " + result + ", " + "0");
        writer.println("b" + ", " + "boolfalse" + boolcounter);
        writer.println("booltrue" + boolcounter + " : ");
        writer.println("li" + ", " + result + ", " + "1");
        writer.println("boolfalse" + boolcounter + " :");
        boolcounter += 1;
    }

    public void mips_precall(){
        writer.println("subu " + Register.sp + ", " + Register.sp + ", " + "8" ); // decrement sp to make space to save ra, fp
        writer.println("sw " + Register.fp + ", " +  "8(" + Register.sp + ")" );  // save fp
        writer.println("sw " + Register.ra + ", " +  "4(" + Register.sp + ")"); // save ra
        writer.println("addiu " + Register.fp + ", " + Register.sp + ", " +  "8"); // set up new fp
        genscope.fp_offset = -4; // reset offset
    }

    public void mips_postreturn(){
        writer.println("move " + Register.sp + ", " + Register.fp ); // pop callee frame off stack
        writer.println("lw " + Register.ra + ", " + "-4(" + Register.fp + ")"); // restore saved ra
        writer.println("lw " + Register.fp + ", "+ "0(" + Register.fp + ")" ); // restore saved fp
        writer.println("jr " + Register.ra);
    }


    public void savetemp(){
        Set<Register> tempreg = new HashSet<Register>();

        for (Register all : Register.tmpRegs){
            for (Register free : freeRegs){
                if (!(all.equals(free))){
                    tempreg.add(all);
                }
            }
        }

        // Deal with local/temps space assignement
        if (tempreg.size() > 0){
            writer.println("subu " + Register.sp + ", " + Register.sp + ", " + 4 * (tempreg.size()));
        }

        for (Register r : tempreg){
            genscope.fp_offset -= 4;
            writer.println("sw " + r + ", "+ genscope.fp_offset+"(" + Register.fp + ")");
        }

        // add to stack of tempregs
        stack_of_temps.add(tempreg);
    }

    public void readtemp(){
        Integer stackoffset = genscope.fp_offset;
        if (stack_of_temps.size() > 0) {
            List<Register> templist = new ArrayList<Register>();
            for (Register r: stack_of_temps.get(stack_of_temps.size() - 1)){
                templist.add(r);
            }
            for (int i = templist.size()-1; i > 0 ; i--) { // reversed list
                writer.println("lw " + templist.get(i) + ", " + stackoffset + "(" + Register.fp + ")");
                stackoffset += 4;
            }
            stack_of_temps.remove(stack_of_temps.size() -1);
        }
    }

}