package ast;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class IntLiteral extends Expr {
    public final Integer IntLiteral;

    public IntLiteral(Integer IntLiteral){
        this.IntLiteral = IntLiteral;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitIntLiteral(this);}
}
