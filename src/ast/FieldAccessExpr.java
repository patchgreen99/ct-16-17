package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class FieldAccessExpr extends Expr{
    public final Expr expr;
    public final String field;
    public VarDecl declaration; // to be filled in by the name analyser
    public StructType structdeclaration;

    public String struct2return;

    public FieldAccessExpr(Expr expr, String field){
        this.expr = expr;
        this.field = field;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitFieldAccessExpr(this);}
}
