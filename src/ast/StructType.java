package ast;

import java.util.List;

/**
 * @author cdubach
 */
public class StructType implements Type {
    public final String structName;
    public final List<VarDecl> params;

    public StructType(String structName, List<VarDecl> params) {
        this.structName = structName;
        this.params = params;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitStructType(this);
    }
}
