package ast;
import java.io.PrintWriter;

public class ASTPrinter implements ASTVisitor<Void> {

    private PrintWriter writer;
    public Boolean html = true;

    public ASTPrinter(PrintWriter writer) {
            this.writer = writer;
    }


    public void htmlprinter(String str){
        //str = str.replace("(", "<ul><li>");
        //str = str.replace(")", "</li></ul>");
        //str = str.replace(",", "</li><li>");
        writer.print(str);
    }

    public void htmlprinter(Integer i){
        writer.print(i);
    }

    public void htmlprinter(Character c){
        writer.print(c);
    }

    public void htmlprinter(Object obj){
        writer.print(obj);
    }


    // ALL VISITOR METHODS


    @Override
    public Void visitFunDecl(FunDecl fd) {
        htmlprinter("FunDecl(");
        fd.type.accept(this);
        htmlprinter(","+fd.name+",");
        for (VarDecl vd : fd.params) {
            vd.accept(this);
            htmlprinter(",");
        }
        fd.block.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitProgram(Program p) {
        htmlprinter("Program(");
        String delimiter = "";
        for (StructType st : p.structTypes) {
            htmlprinter(delimiter);
            delimiter = ",";
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            htmlprinter(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            htmlprinter(delimiter);
            delimiter = ",";
            fd.accept(this);
        }
        htmlprinter(")");
	    writer.flush();
        return null;
    }


    @Override
    public Void visitBlock(Block b) {
        htmlprinter("Block(");
        String delimiter = "";
        for (VarDecl vd : b.vardecls) {
            htmlprinter(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        for (Stmt stmt : b.stmts) {
            htmlprinter(delimiter);
            delimiter = ",";
            stmt.accept(this);
        }
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitVarDecl(VarDecl vd){
        htmlprinter("VarDecl(");
        vd.type.accept(this);
        htmlprinter(","+vd.varName);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        htmlprinter("VarExpr(");
        htmlprinter(v.name);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitBaseType(BaseType bt) {
        htmlprinter(bt);
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        htmlprinter("StructType(");
        htmlprinter(st.structName);
        String delimiter = ",";
        for (VarDecl vd : st.params) {
            htmlprinter(delimiter);
            vd.accept(this);
        }
        htmlprinter(")");
        return null;
    }

    // to complete ...

    @Override
    public Void visitArrayType(ArrayType at) {
        htmlprinter("ArrayType(");
        at.type.accept(this);
        htmlprinter(",");
        htmlprinter(at.IntLiteral);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitFunCallExpr(FunCallExpr fce) {
        htmlprinter("FunCallExpr(");
        htmlprinter(fce.name);
        String delimiter = ",";
        for (Expr expr : fce.params) {
            htmlprinter(delimiter);
            expr.accept(this);
        }
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitOp(Op op) {
        htmlprinter(op);
        return null;
    }

    @Override
    public Void visitBinOp(BinOp bo) {
        htmlprinter("BinOp(");
        bo.expr1.accept(this);
        htmlprinter(",");
        bo.op.accept(this);
        htmlprinter(",");
        bo.expr2.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitAssign(Assign a) {
        htmlprinter("Assign(");
        a.expr1.accept(this);
        htmlprinter(",");
        a.expr2.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitArrayAccessExpr(ArrayAccessExpr aae) {
        htmlprinter("ArrayAccessExpr(");
        aae.expr1.accept(this);
        htmlprinter(",");
        aae.expr2.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitCharLiteral(CharLiteral cl) {
        htmlprinter("ChrLiteral(");
        htmlprinter(cl.CharLiteral);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitExprStmt(ExprStmt es) {
        htmlprinter("ExprStmt(");
        es.expr.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitFieldAccessExpr(FieldAccessExpr fae) {
        htmlprinter("FieldAccessExpr(");
        fae.expr.accept(this);
        htmlprinter(",");
        htmlprinter(fae.field);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitIf(If i) {
        htmlprinter("If(");
        i.bool.accept(this);
        htmlprinter(",");
        i.content1.accept(this);
        if(i.content2!=null){
            htmlprinter(",");
            i.content2.accept(this);
        }
        htmlprinter(")");

        return null;
    }

    @Override
    public Void visitIntLiteral(IntLiteral i) {
        htmlprinter("IntLiteral(");
        htmlprinter(i.IntLiteral);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitPointerType(PointerType pt) {
        htmlprinter("PointerType(");
        pt.type.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitReturn(Return r) {
        htmlprinter("Return(");
        if(r.expr!=null){
            r.expr.accept(this);
        }
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitSizeOfExpr(SizeOfExpr soe) {
        htmlprinter("SizeOfExpr(");
        soe.type.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitStrLiteral(StrLiteral s) {
        htmlprinter("StrLiteral(");
        htmlprinter(s.StrLiteral);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitTypecastExpr(TypecastExpr tce) {
        htmlprinter("TypecastExpr(");
        tce.type.accept(this);
        htmlprinter(",");
        tce.expr.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitValueAtExpr(ValueAtExpr vae) {
        htmlprinter("ValueAtExpr(");
        vae.expr.accept(this);
        htmlprinter(")");
        return null;
    }

    @Override
    public Void visitWhile(While w) {
        htmlprinter("While(");
        w.bool.accept(this);
        htmlprinter(",");
        w.content.accept(this);
        htmlprinter(")");
        return null;
    }
}
