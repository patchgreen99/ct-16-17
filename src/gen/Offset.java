package gen;


/**
 * Created by patrick.green on 09/11/2016.
 */
public class Offset {
    public Boolean isglobal;
    public Boolean ispointer;
    public Integer offset;

    public Offset(Integer offset, Boolean isglobal, Boolean ispointer){
        this.isglobal = isglobal;
        this.ispointer = ispointer;
        this.offset = offset;
    }
}