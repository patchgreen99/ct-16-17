#include "minic-stdlib.h"

void main() {
  int thing;

  print_s("Hello, there.");
  print_s("I'm a \nnewline cat.");
  print_s("\tI'm tabbed in.");
  print_s("I'm \\ a \\ cat.");
  print_s("I'm a \"cat\"");
  print_s("I'm a \f cat.");
  print_s("");
  print_c('\"');
  if (thing == 1) {
    print_s("Uh oh!");
  }
}
