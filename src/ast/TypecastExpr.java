package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class TypecastExpr extends Expr {
    public final Type type;
    public final Expr expr;

    public TypecastExpr(Expr expr, Type type){
        this.expr = expr;
        this.type = type;
    }
    public <T> T accept(ASTVisitor<T> v) {return v.visitTypecastExpr(this);}
}

