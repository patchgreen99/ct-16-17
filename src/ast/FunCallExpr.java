package ast;
import java.util.List;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class FunCallExpr extends Expr{
    public final String name;
    public final List<Expr> params;
    public FunDecl declaration; // to be filled in by the name analyser

    public String struct2return;

    public FunCallExpr(String name, List<Expr> exprs){
        this.name = name;
        this.params = exprs;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitFunCallExpr(this);}
}
