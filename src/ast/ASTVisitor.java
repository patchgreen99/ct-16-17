package ast;

public interface ASTVisitor<T> {
    public T visitBaseType(BaseType bt);
    public T visitStructType(StructType st);
    public T visitBlock(Block b);
    public T visitFunDecl(FunDecl p);
    public T visitProgram(Program p);
    public T visitVarDecl(VarDecl vd);
    public T visitVarExpr(VarExpr v);

    // to complete ... (should have one visit method for each concrete AST node class)
    public T visitArrayType(ArrayType at);
    public T visitFunCallExpr(FunCallExpr fce);
    public T visitOp(Op v);
    public T visitBinOp(BinOp bo);
    public T visitAssign(Assign a);
    public T visitArrayAccessExpr(ArrayAccessExpr aae);
    public T visitCharLiteral(CharLiteral cl);
    public T visitExprStmt(ExprStmt es);
    public T visitFieldAccessExpr(FieldAccessExpr fae);
    public T visitIf(If i);
    public T visitIntLiteral(IntLiteral i);
    public T visitPointerType(PointerType pt);
    public T visitReturn(Return r);
    public T visitSizeOfExpr(SizeOfExpr soe);
    public T visitStrLiteral(StrLiteral s);
    public T visitTypecastExpr(TypecastExpr tce);
    public T visitValueAtExpr(ValueAtExpr vae);
    public T visitWhile(While w);

}
