package sem;


import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Symbol> symbolTable = new HashMap <String, Symbol>();
	
	public Scope(Scope outer) {
		this.outer = outer;
	}
	
	public Scope() { this(null); }

	// All scopes above
	public Symbol lookup(String name) {

		// To be completed...
		Symbol s  = symbolTable.get(name);
		if (s == null && outer != null){
			return outer.lookup(name);
		}
		return s;
	}


	// Only the current scope
	public Symbol lookupCurrent(String name) {
		// To be completed...
		return symbolTable.get(name);
	}


	public void put(FuncSymbol sym) {
		symbolTable.put(sym.name, sym);
	}
	public void put(VarSymbol sym) {
		symbolTable.put(sym.name, sym);
	}
	public void put(StructSymbol sym) {
		symbolTable.put(sym.name, sym);
	}
	public void put(ArraySymbol sym) {
		symbolTable.put(sym.name, sym);
	}
	public void put(PointerSymbol sym) {
		symbolTable.put(sym.name, sym);
	}
}
