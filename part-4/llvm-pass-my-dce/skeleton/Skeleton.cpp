//
// Created by Patrick Green on 14/01/2017.
//

#define DEBUG_TYPE "opCounter"
#include <set>
#include "llvm/IR/CFG.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Module.h"

using namespace std;
using namespace llvm;
namespace {
    DenseMap<const Instruction*,int> MAP;

    struct MyDCE : public FunctionPass {
        static char ID;
        MyDCE() : FunctionPass(ID) {}


        virtual bool runOnFunction(Function &F) {
            errs() << "BEFORE DCE\n";

            // COUNTOP <-----------------


            std::map<std::string, int> opCounter;
            // errs() << "Function " << F.getName() << '\n';
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                        opCounter[i->getOpcodeName()] = 1;
                    } else {
                        opCounter[i->getOpcodeName()] += 1;
                    }
                }
            }
            std::map <std::string, int>::iterator i1 = opCounter.begin();
            std::map <std::string, int>::iterator e1 = opCounter.end();
            while (i1 != e1) {
                errs() << i1->first << ": " << i1->second << "\n";
                i1++;
            }
            // errs() << "\n";
            opCounter.clear();

            errs() << "DCE START\n";

            // DCE <-----------------

            // Calculate the GEN and KILL sets for each instruction

            DenseMap<const BasicBlock*, std::set<const Instruction*>> GEN;
            DenseMap<const BasicBlock*, std::set<const Instruction*>> KILL;


            for (Function::iterator b = F.begin(), e = F.end(); b != e; ++b) {
                //errs() << "BLOCK " << *b << " :\n";
                std::set<const Instruction*> tempgen;
                std::set<const Instruction*> tempkill;

                for (BasicBlock::iterator i = b->begin(), e = b->end(); i != e; ++i) {
                    for (unsigned o = 0 ; o < i->getNumOperands(); o++){
                        Value* v = i -> getOperand(o);
                        if (isa<Instruction>(v)){
                            Instruction *inst = cast<Instruction>(v);
                            if (!tempkill.count(inst)){
                                //errs() << "GEN " << *inst << "\n";
                                tempgen.insert(inst);
                            }

                            // Additional adding for phi instructions
                            if (std::string(inst->getOpcodeName()) == "phi"){
                                for (unsigned dep = 0; dep < inst->getNumOperands(); dep++) {
                                    Value *lowerv = inst->getOperand(dep);
                                    if (isa<Instruction>(lowerv)) {
                                        Instruction *lowerop = cast<Instruction>(lowerv);
                                        if (!tempkill.count(lowerop)) {
                                            //errs() << "GEN " << *lowerop << "\n";
                                            tempgen.insert(lowerop);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //errs() << "KILL " << *i << "\n";
                    tempkill.insert(&*i);
                }

                GEN.insert(std::make_pair(&*b,tempgen));
                KILL.insert(std::make_pair(&*b,tempkill));

            }


            // Calculate the IN and OUT sets between each block

            DenseMap<const BasicBlock*, std::set<const Instruction*>> IN;
            DenseMap<const BasicBlock*, std::set<const Instruction*>> OUT;

            SmallVector<BasicBlock*, 32> stack;
            std::set<BasicBlock*> seen;

            stack.push_back(&*--F.end());

            while (!stack.empty()){
                bool addpredecessors = false;
                BasicBlock *bb = stack.pop_back_val();
                if (seen.find(bb) == seen.end()) {
                    seen.insert(bb);
                    //errs() << "BLOCK " << *bb << "\n";

                    std::set<const Instruction*> uni;
                    // get succeeding instructions
                    for (succ_iterator SI = succ_begin(bb), E = succ_end(bb); SI != E; ++SI){
                        std::set<const Instruction*> temp(IN.lookup(*SI));
                        //errs() << "SUC " << **temp.begin() << " , " << **temp.end() << "\n";
                        uni.insert(temp.begin(),temp.end());
                    }

                    // when the instruction leaving the block is in another block
                    if (uni != OUT.lookup(bb)){
                        addpredecessors = true;
                        OUT.lookup(bb) = uni;
                        IN.lookup(bb).clear();
                        std::set<const Instruction*> insert = IN.lookup(bb);
                        std::set_difference(uni.begin(), uni.end(), KILL.lookup(bb).begin(), KILL.lookup(bb).end(),
                                            std::inserter(insert, insert.end()));
                        //errs() << "PRE " << **GEN.lookup(bb).begin() << " , " << **GEN.lookup(bb).end() << "\n";
                        IN.lookup(bb).insert(GEN.lookup(bb).begin(), GEN.lookup(bb).end());
                    }

                    if (addpredecessors || !IN.count(bb)){
                        for (pred_iterator PI = pred_begin(bb), E = pred_end(bb); PI != E; ++PI) {
                            //errs() << "stack " << *PI << "\n";
                            stack.push_back(*PI);
                        }
                    }

                }
            }

            // Calculate the IN and OUT sets between instructions

            DenseMap<const Instruction*, std::set<const Instruction*>> LIVEIN;
            DenseMap<const Instruction*, std::set<const Instruction*>> LIVEOUT;

            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {

                //errs() << "Block " << *bb << "\n";
                std::set<const Instruction*> templivein;
                std::set<const Instruction*> templiveout;

                templivein = OUT.lookup(&*bb);
                templiveout = OUT.lookup(&*bb);

                for (BasicBlock::reverse_iterator i = bb->rbegin(), e = bb->rend(); i != e; ++i) {
                    //errs() << "Instruction " << *i << "\n";
                    templivein.erase(&*i);

                    for (unsigned o = 0; o < i->getNumOperands(); o++) {
                        Value *v = i->getOperand(o);
                        if(isa<Instruction>(v)){
                            //errs() << "IN " << *v << "\n";
                            Instruction *op = cast<Instruction>(v);
                            templivein.insert(op);

                            if (std::string(op->getOpcodeName()) == "phi"){
                                for (unsigned dep = 0; dep < op->getNumOperands(); dep++) {
                                    Value *lowerv = op->getOperand(dep);
                                    if (isa<Instruction>(lowerv)) {
                                        //errs() << "IN " << *v << "\n";
                                        templivein.insert(cast<Instruction>(lowerv));
                                    }
                                }
                            }


                        }
                    }

                    LIVEIN.insert(std::make_pair(&*i, templivein));
                    LIVEOUT.insert(std::make_pair(&*i, templiveout));

                    templiveout = templivein;
                }

            }



            // Calculate dead instructions

            std::set<const Instruction*> notrequired;
            std::set<const Instruction*> leftafter;

            std::vector<Instruction*> Dead;

            for (Function::iterator bb = --F.end(), e = --F.begin(); bb != e; --bb) {
                //for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {

                for (BasicBlock::reverse_iterator i = bb->rbegin(), e = bb->rend(); i != e; ++i) {
                    //for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {

                    std::set<const Instruction*> out = LIVEOUT.lookup(&*i);
                    std::set<const Instruction*> in = LIVEIN.lookup(&*i);
                    leftafter.clear();

                    // leftafter = s.after - notrequired
                    std::set_difference(out.begin(), out.end(), notrequired.begin(), notrequired.end(),
                                        std::inserter(leftafter, leftafter.end()));

                    if (!out.empty() && leftafter.find(&*i) == leftafter.end() && std::string(i->getOpcodeName()) != "ret" && std::string(i->getOpcodeName()) != "br") {
                        //errs() << *i << " is dead \n";
                        Dead.push_back(&*i);

                        notrequired.clear();
                        // notrequired = s.before - leftafter
                        std::set_difference(in.begin(), in.end(), leftafter.begin(), leftafter.end(),
                                            std::inserter(notrequired, notrequired.end()));
                    } else {
                        notrequired.clear();
                    }

                }
            }

            // Remove dead instructions

            while (!Dead.empty()) {
                Instruction *i = Dead.back();
                Dead.pop_back();
                i->eraseFromParent();
            }


            // COUNTOP <-----------------

            // errs() << "Function " << F.getName() << '\n';
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                        opCounter[i->getOpcodeName()] = 1;
                    } else {
                        opCounter[i->getOpcodeName()] += 1;
                    }
                }
            }
            std::map <std::string, int>::iterator i2 = opCounter.begin();
            std::map <std::string, int>::iterator e2 = opCounter.end();
            while (i2 != e2) {
                errs() << i2->first << ": " << i2->second << "\n";
                i2++;
            }
            // errs() << "\n";
            opCounter.clear();




            errs() << "DCE END\n";
            return false;
        }
    };
}
char MyDCE::ID = 0;
static RegisterPass<MyDCE> X("mydce", "My dead code elimination");


