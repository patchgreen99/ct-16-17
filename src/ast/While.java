package ast;


/**
 * Created by patrick.green on 22/10/2016.
 */
public class While extends Stmt {
    public final Expr bool;
    public final Stmt content;

    public While(Expr bool, Stmt content) {
        this.bool = bool;
        this.content = content;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitWhile(this);
    }
}
