package gen;

import ast.*;

import java.util.*;

/**
 * Created by patrick.green on 09/11/2016.
 */
public class MemoryAllocation implements ASTVisitor<Integer>{

    public Set<String> idents = new HashSet<String>();
    public List<String> strings = new ArrayList<String>();
    public Map<String,StructType> structs = new HashMap<String, StructType>();
    public List<VarDecl> structinstances = new ArrayList<VarDecl>();
    public List<VarDecl> arraylists = new ArrayList<VarDecl>();

    public MemoryAllocation (){
    }

    @Override
    public Integer visitBaseType(BaseType bt) {
        return 0;
    }

    @Override
    public Integer visitStructType(StructType st) {
        if (st.params.size() > 0){
            structs.put(st.structName,st);
        }
        return 0;
    }

    @Override
    public Integer visitBlock(Block b) {
        Integer inc = 0;
        for(VarDecl vd :b.vardecls){
            inc += vd.accept(this);
        }
        for(Stmt s :b.stmts){
            inc += s.accept(this);
        }
        return inc;
    }

    @Override
    public Integer visitFunDecl(FunDecl p) {
        Integer inc = 0;
        for (VarDecl vd : p.params){
            inc += vd.accept(this);
        }
        inc += p.block.accept(this);
                p.incrementstack = inc;
        return 0;
    }


    @Override
    public Integer visitProgram(Program p) {
        for (StructType st :p.structTypes){
            st.accept(this);
        }
        for (VarDecl vd :p.varDecls){
            vd.accept(this);
        }
        for (FunDecl fd :p.funDecls){
            fd.accept(this);
        }

        p.strings = strings;
        p.structinstances = structinstances;
        p.arrayaccess = arraylists;
        return 0;
    }

    @Override
    public Integer visitVarDecl(VarDecl vd) {
                // Deal with struct instances
        if (vd.type instanceof StructType){
            StructType st = structs.get(((StructType) vd.type).structName);
            if (st != null) {
                vd.structdecl = st;
                structinstances.add(vd);
            }
        } else if (vd.type instanceof ArrayType){
            arraylists.add(vd);
        }

        // Deal with memory allocation
        if (idents.contains(vd.varName)){
            return 0;
        } else {
            idents.add(vd.varName);
            return 1;
        }
    }

    @Override
    public Integer visitVarExpr(VarExpr v) {

                if (idents.contains(v.name)){
           return 0;
        } else {
            idents.add(v.name);
            return 1;
        }
    }

    // To be completed...

    @Override
    public Integer visitArrayType(ArrayType at) {
        // To be completed...
        return 0;
    }

    @Override
    public Integer visitFunCallExpr(FunCallExpr fce) {
        Integer inc = 0;
        for(Expr e :fce.params){
            inc += e.accept(this);
        }
        return inc;
    }

    @Override
    public Integer visitOp(Op v) {
        // To be completed...
        return 0;
    }

    @Override
    public Integer visitBinOp(BinOp bo) {
        // To be completed...
        Integer inc = 0;
        inc += bo.expr1.accept(this);
        inc += bo.expr2.accept(this);
        return inc;
    }

    @Override
    public Integer visitAssign(Assign a) {
        // To be completed...
        Integer inc = 0;
        inc += a.expr1.accept(this);
        inc += a.expr2.accept(this);
        return inc;
    }

    @Override
    public Integer visitArrayAccessExpr(ArrayAccessExpr aae) {
        // To be completed...


        Integer inc = 0;
        inc += aae.expr1.accept(this);
        inc += aae.expr2.accept(this);
        return inc;
    }

    @Override
    public Integer visitCharLiteral(CharLiteral cl) {
        // To be completed...
                return 1;
    }

    @Override
    public Integer visitExprStmt(ExprStmt es) {
        // To be completed...
        return es.expr.accept(this);
    }

    @Override
    public Integer visitFieldAccessExpr(FieldAccessExpr fae) {
        // To be completed...
        // Todo with global pointer I think

        return 0;
    }

    @Override
    public Integer visitIf(If i) {
        // To be completed...
        Integer inc = 0;
        i.bool.accept(this);
        inc += i.content1.accept(this);
        if (i.content2 != null){
            inc += i.content2.accept(this);
        }
        return inc;
    }

    @Override
    public Integer visitIntLiteral(IntLiteral i) {
        // To be completed...
                return 1;
    }

    @Override
    public Integer visitPointerType(PointerType pt) {
        // To be completed...
        return 0;
    }

    @Override
    public Integer visitReturn(Return r) {
        // To be completed...
        Integer inc = 0;
        if (r.expr != null){
            inc += r.expr.accept(this);
        }
        return inc;
    }

    @Override
    public Integer visitSizeOfExpr(SizeOfExpr soe) {
        // To be completed...
                return 1;
    }

    @Override
    public Integer visitStrLiteral(StrLiteral s) {
        // To be completed...
        // Todo deal with global pointer
        // STRING ESCAPE CHARACTERS
        String str = s.StrLiteral;
        str = str.replaceAll("\\n","\\\\n");
//        str = str.replaceAll("\\t","\\\\t");
//        str = str.replaceAll("\\b","\\\\b");
//        str = str.replaceAll("\\r","\\\\r");
//        str = str.replaceAll("\\f","\\\\f");
//        str = str.replaceAll("\\'","\\\\'");
//        str = str.replaceAll("\\\\","\\\\\\\\");
//        str = str.replaceAll("\\\"","\\\\\"");

        strings.add(str);
        return 0;
    }

    @Override
    public Integer visitTypecastExpr(TypecastExpr tce) {
        // To be completed...
        Integer inc = 0;
        inc += tce.type.accept(this);
        inc += tce.expr.accept(this);
        return inc;
    }

    @Override
    public Integer visitValueAtExpr(ValueAtExpr vae) {
        return vae.expr.accept(this);
    }

    @Override
    public Integer visitWhile(While w) {
        // To be completed...
        Integer inc = 0;
        inc += w.bool.accept(this);
        inc += w.content.accept(this);
        return inc;
    }


    /*
    Type curtype = vd.type;
    String name = vd.varName;
        if (curtype instanceof StructType){
        String structname = ((StructType) curtype).structName;
        for (VarDecl param : lookupstruct.get(structname).params){
            writer.print(name + "_" + structname + "_");
            param.accept(this);
        }

    } else if (curtype instanceof ArrayType) {
        writer.print(name + ": .word" + "0 : " + ((ArrayType) curtype).IntLiteral + "\n");

    } else {
        writer.print(name + ": .word" + "\n");

    }
    */

//    public String structreturns(Expr e){
//        while (!(e instanceof VarExpr)){
//            if (e instanceof ArrayAccessExpr){
//
//            } else if (e instanceof FieldAccessExpr){
//                fi
//            } else if (e instanceof FunCallExpr){
//                ((FunCallExpr) e).declaration.
//            }
//        }
//        return
//    }



}
