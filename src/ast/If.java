package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class If extends Stmt{
    public final Expr bool;
    public final Stmt content1;
    public Stmt content2;

    public If(Expr bool, Stmt content1) {
        this.bool = bool;
        this.content1 = content1;
    }

    public If(Expr bool, Stmt content1, Stmt content2) {
        this.bool = bool;
        this.content1 = content1;
        this.content2 = content2;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitIf(this);
    }
}

