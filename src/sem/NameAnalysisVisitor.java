package sem;

import ast.*;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {
    public Scope scope;
    //public Map<String, Symbol> structfields = new HashMap<String, Symbol>();
    public StructType structfields;
    public List<VarDecl> funcparams = new ArrayList<VarDecl>();

    NameAnalysisVisitor(Scope scope) {
        this.scope = scope ;
        List<VarDecl> params = new ArrayList<VarDecl>();
        params.add(new VarDecl(new PointerType(BaseType.CHAR),"s"));
        scope.put(new FuncSymbol(new FunDecl(BaseType.VOID,"print_s",params,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));

        List<VarDecl> params1 = new ArrayList<VarDecl>();
        params1.add(new VarDecl(BaseType.INT,"i"));
        scope.put(new FuncSymbol(new FunDecl(BaseType.VOID,"print_i",params1,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));

        List<VarDecl> params2 = new ArrayList<VarDecl>();
        params2.add(new VarDecl(BaseType.CHAR,"c"));
        scope.put(new FuncSymbol(new FunDecl(BaseType.VOID,"print_c",params2,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));

        List<VarDecl> params3 = new ArrayList<VarDecl>();
        scope.put(new FuncSymbol(new FunDecl(BaseType.CHAR,"read_c",params3,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));

        List<VarDecl> params4 = new ArrayList<VarDecl>();
        scope.put(new FuncSymbol(new FunDecl(BaseType.INT,"read_i",params4,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));

        List<VarDecl> params5 = new ArrayList<VarDecl>();
        params5.add(new VarDecl(BaseType.INT,"size"));
        scope.put(new FuncSymbol(new FunDecl(new PointerType(BaseType.VOID),"mcmalloc",params5,new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>()))));
    }

    @Override
    public Void visitBaseType(BaseType bt) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        Symbol s = scope.lookupCurrent(st.structName);
        if (s != null)
            error(st.structName + "  ............ struct already declared");
        else
            scope.put(new StructSymbol(st.structName,st));
        return null;
    }

    @Override
    public Void visitBlock(Block b) {
        Scope oldScope = scope ;
        scope = new Scope(scope);

        // check the function parameters from the function, now correctly inside the block
        for (VarDecl param: funcparams){
            param.accept(this);
        }

        // Clear all function parameters straight after
        funcparams = new ArrayList<VarDecl>();

        for (VarDecl vd : b.vardecls) {
            vd.accept(this);
        }

        for (Stmt st : b.stmts) {
            st.accept(this);
        }

        // visit the children

        scope = oldScope;
        return null;
    }

    @Override
    public Void visitFunDecl(FunDecl p) {
        Symbol s = scope.lookupCurrent(p.name);
        if (s != null)
            error(p.name + "  ............ function already declared");
        else
            scope.put(new FuncSymbol(p));

        // Parameters of method, save them all to functparams
        for (VarDecl vd : p.params) {

            // functparams must be called inside the new scope
            // funcparams are saved to an additional HashMap
            funcparams.add(vd);
        }

        // The method itself
        p.block.accept(this);


        return null;
    }


    @Override
    public Void visitProgram(Program p) {
        // To be completed...
        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Void visitVarDecl(VarDecl vd) {
        Symbol s = scope.lookupCurrent(vd.varName);
        if (s != null) {
            error(vd.varName + " has already been declared");

        } else if (vd.type instanceof ArrayType) { // An arraytype
            scope.put(new ArraySymbol(vd));

        } else if (vd.type instanceof StructType) { //struct instance
            Symbol old_struct= scope.lookup(((StructType) vd.type).structName);
            if (old_struct == null){
                error("struct has not been declared for use as an instance");
            } else {
                try {
                    StructType structinstance = ((StructSymbol) old_struct).structdecl;
                    scope.put(new StructSymbol(vd.varName, structinstance));
                } catch (java.lang.ClassCastException e){
                    error("variable not declared as a struct");
                }
            }

        } else if (vd.type instanceof  PointerType){ // Pointer
            scope.put(new PointerSymbol(vd));

        } else {
            scope.put(new VarSymbol(vd));}

        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        Symbol vs = scope.lookup(v.name);
        if (vs == null)
            error (v.name + "  ............ has not been declared");

        // Struct
        else if (vs instanceof StructSymbol) {
            v.structdeclaration= ((StructSymbol) vs).structdecl;
            // fill the struct variable list now. It is deleted once the line with field accessing is passed
            structfields = v.structdeclaration;


        // Array
        } else if (vs instanceof ArraySymbol) {
            v.declaration = ((ArraySymbol) vs).array;

        // Pointer
        //} else if (vs instanceof PointerSymbol) {   must work out how to make this warning work
        //    error(((PointerSymbol) vs).name + " was declared as a pointer");

        } else if (vs instanceof PointerSymbol) {
            v.declaration = ((PointerSymbol) vs).vardecl;
            //was_value_at = true;

        } else if (! (vs instanceof  VarSymbol)) {
            error(v.name + "  ............ is not a variable or struct");

        // A variable
        } else {
            // Save the declaration in the node, variable
            try {
                v.declaration = ((VarSymbol) vs).vardecl;
            } catch (java.lang.ClassCastException e) {
                error(v.name + "  ............ is not a variable or struct");
            }
        }

        //} else // everything is fine , record var . decl . v.vd = ((VarSymbol) vs).vd;

        return null;
    }

    // To be completed...

    @Override
    public Void visitArrayType(ArrayType at) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitFunCallExpr(FunCallExpr fce) {
        // To be completed..

        // Check function exits

        Symbol s = scope.lookup(fce.name);
        if (s instanceof FuncSymbol){
            // Save the declaration in the node
            fce.declaration = ((FuncSymbol) s).funcdecl;
        } else {
            error(fce.name + "  ............ function does not exists");
        }

        // Check parameters
        for (Expr expr :fce.params){
            expr.accept(this);
        }


        return null;
    }

    @Override
    public Void visitOp(Op v) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitBinOp(BinOp bo) {
        // To be completed...
        bo.expr1.accept(this);
        bo.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitAssign(Assign a) {
        // To be completed...
        a.expr1.accept(this);
        a.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitArrayAccessExpr(ArrayAccessExpr aae) {
        // To be completed...
        aae.expr1.accept(this);
        aae.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitCharLiteral(CharLiteral cl) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitExprStmt(ExprStmt es) {
        // To be completed...
        es.expr.accept(this);
        return null;
    }

    @Override
    public Void visitFieldAccessExpr(FieldAccessExpr fae) {
        // To be completed...

        // Dealing with structs
        fae.expr.accept(this);



        String field = fae.field;
        fae.structdeclaration = structfields;

        for (VarDecl vd: fae.structdeclaration.params){
            if (vd.varName.equals(field)){
                fae.declaration = vd;
            }
        }
        if (fae.declaration == null){
            error(field + " is not a field of the struct or the struct does not exist");
        }

            // Struct fields are cleared again
        structfields = new StructType("",new ArrayList<VarDecl>());
        return null;
    }

    @Override
    public Void visitIf(If i) {
        // To be completed...
        i.bool.accept(this);
        i.content1.accept(this);
        if (i.content2 != null){
            i.content2.accept(this);
        }
        return null;
    }

    @Override
    public Void visitIntLiteral(IntLiteral i) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitPointerType(PointerType pt) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitReturn(Return r) {
        // To be completed...
        if (r.expr != null){
            r.expr.accept(this);
        }
        return null;
    }

    @Override
    public Void visitSizeOfExpr(SizeOfExpr soe) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitStrLiteral(StrLiteral s) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitTypecastExpr(TypecastExpr tce) {
        // To be completed...
        tce.expr.accept(this);
        return null;
    }

    @Override
    public Void visitValueAtExpr(ValueAtExpr vae) {
        vae.expr.accept(this);
        // Save the declaration


        return null;
    }

    @Override
    public Void visitWhile(While w) {
        // To be completed...
        w.bool.accept(this);
        w.content.accept(this);
        return null;
    }


}
