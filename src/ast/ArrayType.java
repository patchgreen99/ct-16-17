package ast;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class ArrayType implements Type {
    public final Type type;
    public final Integer IntLiteral;

    public ArrayType(Type type, Integer IntLiteral) {
        this.type = type;
        this.IntLiteral = IntLiteral;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitArrayType(this);
    }
}

