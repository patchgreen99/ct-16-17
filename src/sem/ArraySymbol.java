package sem;
import ast.Type;
import ast.VarDecl;

/**
 * Created by patrick.green on 24/10/2016.
 */
public class ArraySymbol extends Symbol {
    public VarDecl array;
    public String name;
    public Type type;

    ArraySymbol(VarDecl vd){
        this.array = vd;
        this.name = vd.varName;
        this.type = array.type;
    }

}
