package sem;

import ast.*;
import java.lang.IndexOutOfBoundsException;
import java.util.ArrayList;
import java.util.List;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {

	@Override
	public Type visitBaseType(BaseType bt) {
		// To be completed...
		return bt;
	}

	@Override
	public Type visitStructType(StructType st) {
		// To be completed...
		return st;
	}

	@Override
	public Type visitBlock(Block b) {
		// To be completed...
		List<Type> returntypes = new ArrayList<Type>();

		for (VarDecl vd: b.vardecls){
			vd.accept(this);
		}

		for (Stmt st: b.stmts){
			Type t =st.accept(this);
			if (t != null) {
				returntypes.add(t);
			}
		}

		// Different duplicate return types
		for (int i=1 ; i < returntypes.size() ; i++){
			if (returntypes.get(i-1) != returntypes.get(i) && returntypes.get(i-1) != null && returntypes.get(i) != null) {
				error("multiple return statements do not have the same type");
			}
		}

		if (returntypes.size() > 0){
			return returntypes.get(returntypes.size()-1);
		} else {
			return null;
		}
	}

	@Override
	public Type visitFunDecl(FunDecl p) {
		// To be completed...
		for (VarDecl vd : p.params){
			vd.accept(this);
		}

		// Must return the correct type
		Type t = p.block.accept(this);
		Type functype = p.type;
		if (t == null){
			t = BaseType.VOID;
		}

		// Go deeper

		// Check all lower types to see if correct
		while (! (functype instanceof BaseType) && !(t instanceof BaseType) && functype != null && t != null){
			// Check

			if (! (functype.getClass().equals( t.getClass()))){
				error("incorrect assigning of types");
				return null;
			}

			// Go deeper
			if (functype instanceof ArrayType && t instanceof ArrayType){
				try {
					functype = ((ArrayType) functype).type;
					t = ((ArrayType) t).type;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}

			} else if (functype instanceof PointerType && t instanceof PointerType) {
				try {
					functype = ((PointerType) functype).type;
					t = ((PointerType) t).type;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}

			} else if (functype instanceof StructType && t instanceof StructType) {
				String name1 = null;
				String name2 = null;
				try {
					name1 = ((StructType) functype).structName;
					name2 = ((StructType) t).structName;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}

				if (! name1.equals(name2) ){
					error("incorrect assigning of types");
				} else {
					return null;
				}
			}
		}

		// Should be both base types now
		if (functype != t){
			error("incorrect assigning of types");
		}


		return null;
	}


	@Override
	public Type visitProgram(Program p) {
		// To be completed...
		for (StructType st :p.structTypes){
			st.accept(this);
		}
		for (VarDecl vd :p.varDecls){
			vd.accept(this);
		}
		for (FunDecl fd :p.funDecls){
			fd.accept(this);
		}

		return null;
	}

	@Override
	public Type visitVarDecl(VarDecl vd) {
		// To be completed...
		if (vd.type == BaseType.VOID){
			error("the variable " + vd.varName + " has the incorrect type");
		}
		return null;
	}

	@Override
	public Type visitVarExpr(VarExpr v) {
		// To be completed...
		if (v.declaration != null) {
			if (v.declaration.type != null) {
				v.type = v.declaration.type.accept(this);
			} else {
				error("variable "+ v.name  + " has not been declared");
			}
		}
		return v.type;
	}

	// To be completed...

	@Override 
 	public Type visitArrayType(ArrayType at) {
		// To be completed...
		return at;
	}

	@Override 
 	public Type visitFunCallExpr(FunCallExpr fce) {
		// To be completed...

		// Handle function parameters
		FunDecl funcdecl = fce.declaration;

		if (funcdecl != null) {

			try {
				for (int i = 0; i < funcdecl.params.size(); i++) {
					VarDecl variable = fce.declaration.params.get(i);
					Type declaredas = variable.type;
					Type usedas = fce.params.get(i).accept(this);
					if (declaredas != usedas && !(declaredas instanceof PointerType) && !(usedas instanceof PointerType) || (declaredas instanceof PointerType && usedas instanceof PointerType) && ((PointerType) declaredas).type != ((PointerType) usedas).type) { // Second bit for the pointer case
						error("parameter has the wrong type for function " + fce.name);
					}
				}

				// To little
				if (funcdecl.params.size() != fce.params.size()){
					error("function " + fce.name + " has an incorrect number of parameters");
				}
			// To many
			} catch (IndexOutOfBoundsException e) {
				error("function " + fce.name + " has an incorrect number of parameters");
			}

			return fce.declaration.type;
		} else {
			return null;
		}
	}

	@Override 
 	public Type visitOp(Op v) {
		// To be completed...
		return null;
	}

	@Override 
 	public Type visitBinOp(BinOp bo) {
		// To be completed...
		Type lhsT = bo.expr1.accept(this);
		Type rhsT = bo.expr2.accept(this);
		if (bo.op == Op.MUL || bo.op == Op.DIV || bo.op == Op.ADD || bo.op == Op.SUB || bo.op == Op.MOD || bo.op == Op.OR || bo.op == Op.AND || bo.op == Op.GT || bo.op == Op.LT || bo.op == Op.GE || bo.op == Op.LE){
			if (lhsT == BaseType.INT && rhsT == BaseType.INT){
				bo.type = BaseType.INT;
				return BaseType.INT;

			} else {
				error(lhsT + " , " + rhsT + " types on the binop are not the same");
			}

		} else if (bo.op == Op.NE || bo.op == Op.EQ) {
			// Go deeper

			// Check all lower types to see if correct
			while (! (lhsT instanceof BaseType) && !(rhsT instanceof BaseType) && lhsT != null && rhsT != null){
				// Check

				if (! (lhsT.getClass().equals( rhsT.getClass()))){
					error("incorrect assigning of types");
					return null;
				}

				// Go deeper
				if (lhsT instanceof ArrayType && rhsT instanceof ArrayType){
					try{
						lhsT = ((ArrayType) lhsT).type;
						rhsT = ((ArrayType) rhsT).type;
					} catch (java.lang.NullPointerException e) {
						error("incorrect assigning of types");
					}

				} else if (lhsT instanceof PointerType && rhsT instanceof PointerType){
					try {
						lhsT = ((PointerType) lhsT).type;
						rhsT = ((PointerType) rhsT).type;
					} catch (java.lang.NullPointerException e) {
						error("incorrect assigning of types");
					}

				} else if (lhsT instanceof StructType && rhsT instanceof StructType) {
					String name1 = null;
					String name2 = null;
					try {
						name1 = ((StructType) lhsT).structName;
						name2 = ((StructType) rhsT).structName;
					} catch (java.lang.NullPointerException e) {
						error("incorrect assigning of types");
					}

					if (! name1.equals(name2) ){
						error("incorrect assigning of types");
					} else {
						return null;
					}
				}
			}

			// Should be both base types now
			if (lhsT != rhsT){
				error("incorrect assigning of types");
			}

		}

		// Boolean is 1,0
		return BaseType.INT;
	}

	@Override 
 	public Type visitAssign(Assign a) {
		// To be completed...

		// The lhs and rhs must match and not void or ArrayType

		Type lhsT = a.expr1.accept(this);
		Type rhsT = a.expr2.accept(this);

		if ( lhsT == BaseType.VOID || lhsT instanceof ArrayType ){
			error("incorrect assigning of types");
		}

		// The lhs can only be ArrayAccess VarExp FieldAccess ValueAtExp
		if ( a.expr1 instanceof IntLiteral || a.expr1 instanceof StrLiteral || a.expr1 instanceof CharLiteral || a.expr1 instanceof FunCallExpr || a.expr1 instanceof BinOp || a.expr1 instanceof TypecastExpr || a.expr1 instanceof SizeOfExpr){
			error("incorrect lhs to be assigned a variable");
		}

		// Check all lower types to see if correct
		while (! (lhsT instanceof BaseType) && !(rhsT instanceof BaseType) && lhsT != null && rhsT != null){
			// Check
			if (! (lhsT.getClass().equals( rhsT.getClass()))){
				error("incorrect assigning of types");
				return null;
			}

			// Go deeper
			if (lhsT instanceof ArrayType && rhsT instanceof ArrayType){
				try{
					lhsT = ((ArrayType) lhsT).type;
					rhsT = ((ArrayType) rhsT).type;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}


			} else if (lhsT instanceof PointerType && rhsT instanceof PointerType){
				try{
					lhsT = ((PointerType) lhsT).type;
					rhsT = ((PointerType) rhsT).type;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}

			} else if (lhsT instanceof StructType && rhsT instanceof StructType) {
				String name1 = null;
				String name2 = null;
				try {
					name1 = ((StructType) lhsT).structName;
					name2 = ((StructType) rhsT).structName;
				} catch (java.lang.NullPointerException e) {
					error("incorrect assigning of types");
				}

				if (! name1.equals(name2) ){
					error("incorrect assigning of types");
				} else {
					return null;
				}
			}
		}

		// Should be both base types now
	    if (lhsT != rhsT){
			error("incorrect assigning of types");
		}

		return null;
	}

	@Override 
 	public Type visitArrayAccessExpr(ArrayAccessExpr aae) {
		// To be completed..
		if (aae.expr2.accept(this) != BaseType.INT){
			error("array index not an integer");
		}
		Type type = aae.expr1.accept(this);
		// Array access only done to pointer or declared array
		if (! (type instanceof PointerType) && ! (type instanceof ArrayType)){
			error("cannot array access a "+ type);
			return null;
		} else {
			// Return the lower type
			try {
				return ((ArrayType) type).type;
			} catch (java.lang.ClassCastException e){
				return ((PointerType) type).type;
			}
		}
	}

	@Override 
 	public Type visitCharLiteral(CharLiteral cl) {
		// To be completed...
		return BaseType.CHAR;
	}

	@Override 
 	public Type visitExprStmt(ExprStmt es) {
		// To be completed...
		es.expr.accept(this);
		return null;
	}

	@Override 
 	public Type visitFieldAccessExpr(FieldAccessExpr fae) {  // NEED TO DO
		// To be completed...

		// Must return the type as stated in the struct
		VarDecl structdecl = fae.declaration;
		if (structdecl != null){
			return fae.declaration.type;
		} else {
			return null;
		}

	}

	@Override 
 	public Type visitIf(If i) {
		// To be completed...

		Type returntype2 = null;

		// Check constraint is boolean
		if (i.bool.accept(this) != BaseType.INT){
			error("If statements constraint has an incorrect type");
		}

		Type returntype1 = i.content1.accept(this);

		if (i.content2 != null){
			returntype2 = i.content2.accept(this);
		}

		// Check return types are the same
		if (returntype1 != returntype2 && returntype1 != null && returntype2 != null){
			error("multiple return statements do not have the same type");
		}

		return returntype1;
	}

	@Override 
 	public Type visitIntLiteral(IntLiteral i) {
		// To be completed...
		return BaseType.INT;
	}

	@Override 
 	public Type visitPointerType(PointerType pt) {
		// To be completed...
		return pt;
	}

	@Override 
 	public Type visitReturn(Return r) {
		// To be completed...
		if (r.expr != null){
			r.type = r.expr.accept(this);
		} else {
			r.type = BaseType.VOID;
		}
		return r.type;
	}

	@Override 
 	public Type visitSizeOfExpr(SizeOfExpr soe) {
		// To be completed...
		return BaseType.INT;
	}

	@Override 
 	public Type visitStrLiteral(StrLiteral s) {
		// To be completed...
		return new ArrayType(BaseType.CHAR,0);
	}

	@Override 
 	public Type visitTypecastExpr(TypecastExpr tce) {
		// To be completed...
		// Char to Int
		// Array to Pointer
		// Pointer to Pointer

		// An array is cast to a pointer
		Type type = tce.expr.accept(this);
		if ( type instanceof ArrayType){
			return new PointerType(((ArrayType) type).type);
		} else {
			return tce.type;
		}

	}

	@Override 
 	public Type visitValueAtExpr(ValueAtExpr vae) {
		// To be completed...
		// Pointer call !!

		vae.type = vae.expr.accept(this);

		if (vae.type != null) {
			try {
				return ((PointerType) vae.type).type;
			} catch (java.lang.ClassCastException e) {
				error("variable has not been declared as a pointer");
				return vae.type;
			}
		} else {
			return null;
		}

	}

	@Override 
 	public Type visitWhile(While w) {
		// To be completed...

		// Check constraint is boolean
		if (w.bool.accept(this) != BaseType.INT){
			error("While loop constraint has an incorrect type");
		}

		return w.content.accept(this);
	}


}
