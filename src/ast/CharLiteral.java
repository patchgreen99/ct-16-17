package ast;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class CharLiteral extends Expr {
    public final Character CharLiteral;

    public CharLiteral(Character CharLiteral){
        this.CharLiteral = CharLiteral;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitCharLiteral(this);}
}
