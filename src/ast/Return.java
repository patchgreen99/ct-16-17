package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class Return extends Stmt {
    public Expr expr;
    public Type type;

    public Return() {
    }

    public Return(Expr expr) {
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitReturn(this);
    }
}


