package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author cdubach
 */
public class Tokeniser {

    private Scanner scanner;

    private int error = 0;

    public int getErrorCount() {
        return this.error;
    }

    public Tokeniser(Scanner scanner) {
        this.scanner = scanner;
    }

    public boolean wasWhiteSpace;

    private void error(char c, int line, int col) {
        System.out.println("Lexing error: unrecognised character (" + c + ") at " + line + ":" + col);
        error++;
    }


    public Token nextToken() {
        Token result;
        try {
            result = next();
        } catch (EOFException eof) {
            // end of file, nothing to worry about, just return EOF token
            return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            // something went horribly wrong, abort
            System.exit(-1);
            return null;
        }
        return result;
    }

    /*
     * To be completed
     */
    private Token next() throws IOException {

        int line = scanner.getLine();
        int column = scanner.getColumn();

        // get the next character
        char c = scanner.next();

        // skip white spaces
        if (Character.isWhitespace(c)){
            wasWhiteSpace = true;
            return next();
        } else {
            wasWhiteSpace = false;
        }


        // recognises the plus operator
        if (c == '+')
            return new Token(TokenClass.PLUS, line, column);

        // single obvious Tokens
        switch (c) {
            case '-':
                return new Token(TokenClass.MINUS, line, column);
            case '*':
                return new Token(TokenClass.ASTERIX, line, column);
            case '%':
                return new Token(TokenClass.REM, line, column);
            case '.':
                return new Token(TokenClass.DOT, line, column);
            case ',':
                return new Token(TokenClass.COMMA, line, column);
            case ';':
                return new Token(TokenClass.SC, line, column);
            case '{':
                return new Token(TokenClass.LBRA, line, column);
            case '}':
                return new Token(TokenClass.RBRA, line, column);
            case '(':
                return new Token(TokenClass.LPAR, line, column);
            case ')':
                return new Token(TokenClass.RPAR, line, column);
            case '[':
                return new Token(TokenClass.LSBR, line, column);
            case ']':
                return new Token(TokenClass.RSBR, line, column);
        }

        // double obvious Tokens
        if (c == '!') {
            if (scanner.peek() == '=') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.NE, line, column);
            }
        }

        if (c == '|') {
            if (scanner.peek() == '|') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.OR, line, column);
            }
        }

        if (c == '&') {
            if (scanner.peek() == '&') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.AND, line, column);
            }
        }

        if (c == '=') {
            if (scanner.peek() == '=') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.EQ, line, column);
            } else {
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.ASSIGN, line, column);
            }
        }

        if (c == '<') {
            if (scanner.peek() == '=') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.LE, line, column);
            } else {
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.LT, line, column);
            }
        }

        if (c == '>') {
            if (scanner.peek() == '=') {
                scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.GE, line, column);
            } else {
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.GT, line, column);
            }
        }

        if (c == '#') {
            char[] include = "include".toCharArray();
            int i = 0;
            while (include[i] == scanner.peek()) {
                c = scanner.next();
                i++;
                if (i == include.length) {
                    line = scanner.getLine();
                    column = scanner.getColumn();
                    return new Token(TokenClass.INCLUDE, line, column);
                }
            }
        }


        // COMMENTS ARE NOT TOKENISED
        if (c == '/') {
            if (scanner.peek() == '/') {
                c = scanner.next();
                while (scanner.next() != '\n') {
                }
                return next();
            } else if (scanner.peek() == '*'){
                c = scanner.next();
                while (true) {
                    if (scanner.peek() == '*') {
                        c = scanner.next();
                        if (scanner.peek() == '/') {
                            c = scanner.next();
                            break;
                        }
                    }
                    c = scanner.next();
                }
                return next();
            } else {
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.DIV, line, column);
            }
        }


        // STRING_LITERAL
        if (c == '"') {
            String value = "";
            while (scanner.peek() != '"') {
                if (scanner.peek() == '\\') {   // Escape character the only ones aloud are
                    scanner.next();

                    if (scanner.peek() == 't'){
                        value += '\t';
                        scanner.next();}
                    else if (scanner.peek() =='b'){
                        value += '\b';
                        scanner.next();}
                    else if (scanner.peek() =='n'){
                        value += '\n';
                        scanner.next();}
                    else if (scanner.peek() =='r'){
                        value += '\r';
                        scanner.next();}
                    else if (scanner.peek() =='f'){
                        value += '\f';
                        scanner.next();}
                    else if (scanner.peek() =='\'') {
                        value += "'";
                        scanner.next();} // still broken
                    else if (scanner.peek() == '"') {
                        value += '\"';
                        scanner.next();} // still broken
                    else if (scanner.peek() =='\\'){
                        value += '\\';
                        scanner.next();}
                    else {
                        line = scanner.getLine();
                        column = scanner.getColumn();
                        error(c, line, column);
                    }
                } else {
                    value += scanner.next();
                }
            }
            scanner.next();
            line = scanner.getLine();
            column = scanner.getColumn();
            return new Token(TokenClass.STRING_LITERAL, value, line, column);
        }


        // INT_LITERAL
        if (Character.isDigit(c)) {
            String value = "";
            value += c;
            while (Character.isDigit(scanner.peek())) {
                value += scanner.next();
            }
            line = scanner.getLine();
            column = scanner.getColumn();
            return new Token(TokenClass.INT_LITERAL, value, line, column);
        }

        // CHAR_LITERAL  MIGHT NEED FIDDLING WITH
        if (c == '\'') {
            String value= "";
            if (scanner.peek() == '\\'){
                scanner.next();
                if (scanner.peek() == 't'){
                    value += '\t';
                    scanner.next();}
                else if (scanner.peek() =='b'){
                    value += '\b';
                    scanner.next();}
                else if (scanner.peek() =='n'){
                    value += '\n';
                    scanner.next();}
                else if (scanner.peek() =='r'){
                    value += '\r';
                    scanner.next();}
                else if (scanner.peek() =='f'){
                    value += '\f';
                    scanner.next();}
                else if (scanner.peek() =='\'') {
                    value += '\'';
                    scanner.next();} // still broken
                else if (scanner.peek() == '"') {
                    value += '\"';
                    scanner.next();} // still broken
                else if (scanner.peek() =='\\'){
                    value += '\\';
                    scanner.next();}
                else {
                    line = scanner.getLine();
                    column = scanner.getColumn();
                    error(c, line, column);
                }


            // APPARENTLY ALL CHARS ARE ALOUD

            } else { //if (Character.isLetterOrDigit(scanner.peek()) || ! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_' || scanner.peek() == '\'' || scanner.peek() == '"' || scanner.peek() == '\\' || scanner.peek() == '.' || scanner.peek() == ',' || scanner.peek() == '_') {
                value += scanner.next();
            }

            if (scanner.peek() == '\'') {
                c = scanner.next();
                line = scanner.getLine();
                column = scanner.getColumn();
                return new Token(TokenClass.CHAR_LITERAL, value, line, column);
            }
        }


        // IDENTIFIER, TYPES and KEYWORDS
        if (Character.isLetter(c) || c == '_') {
            String value = "";
            value += c;
            if (c == 'c') {
                char[] str = "char".toCharArray();
                int i = 1;
                while (str[i] == scanner.peek()) {
                    value += scanner.next();
                    i++;
                    if (i == str.length) {
                        if (! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_') {
                            column = scanner.getColumn();
                            line = scanner.getLine();
                            return new Token(TokenClass.CHAR, line, column);
                        }
                        break;
                    }
                }
            } else if (c == 'e') {
                char[] str = "else".toCharArray();
                int i = 1;
                while (str[i] == scanner.peek()) {
                    value += scanner.next();
                    i++;
                    if (i == str.length) {
                        column = scanner.getColumn();
                        line = scanner.getLine();
                        return new Token(TokenClass.ELSE, line, column);
                    }
                }
            } else if (c == 'i') {
                if (scanner.peek() == 'f') {
                    value += scanner.next();
                    column = scanner.getColumn();
                    line = scanner.getLine();
                    return new Token(TokenClass.IF, line, column);
                } else if (scanner.peek() == 'n') {
                    value += scanner.next();
                    char[] str = "int".toCharArray();
                    int i = 2;
                    while (str[i] == scanner.peek()) {
                        value += scanner.next();
                        i++;
                        if (i == str.length) {
                            if (! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_') {
                                column = scanner.getColumn();
                                line = scanner.getLine();
                                return new Token(TokenClass.INT, line, column);
                            }
                            break;
                        }
                    }
                }

            } else if (c == 'r') {
                char[] str = "return".toCharArray();
                int i = 1;
                while (str[i] == scanner.peek()) {
                    value += scanner.next();
                    i++;
                    if (i == str.length) {
                        if (! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_') {
                            column = scanner.getColumn();
                            line = scanner.getLine();
                            return new Token(TokenClass.RETURN, line, column);
                        }
                        break;
                    }
                }
            } else if (c == 's') {
                if (scanner.peek() == 't') {
                    value += scanner.next();
                    char[] str = "struct".toCharArray();
                    int i = 2;
                    while (str[i] == scanner.peek()) {
                        value += scanner.next();
                        i++;
                        if (i == str.length) {
                            if (! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_') {
                                column = scanner.getColumn();
                                line = scanner.getLine();
                                return new Token(TokenClass.STRUCT, line, column);
                            }
                            break;
                        }
                    }
                } else if (scanner.peek() == 'i') {
                    value += scanner.next();
                    char[] str = "sizeof".toCharArray();
                    int i = 2;
                    while (str[i] == scanner.peek()) {
                        value += scanner.next();
                        i++;
                        if (i == str.length) {
                            column = scanner.getColumn();
                            line = scanner.getLine();
                            return new Token(TokenClass.SIZEOF, line, column);
                        }
                    }
                }

            } else if (c == 'v') {
                char[] str = "void".toCharArray();
                int i = 1;
                while (str[i] == scanner.peek()) {
                    value += scanner.next();
                    i++;
                    if (i == str.length) {
                        if (! Character.isLetterOrDigit(scanner.peek()) && scanner.peek() != '_') {
                            column = scanner.getColumn();
                            line = scanner.getLine();
                            return new Token(TokenClass.VOID, line, column);
                        }
                        break;
                    }
                }
            } else if (c == 'w') {
                char[] str = "while".toCharArray();
                int i = 1;
                while (str[i] == scanner.peek()) {
                    value += scanner.next();
                    i++;
                    if (i == str.length) {
                        column = scanner.getColumn();
                        line = scanner.getLine();
                        return new Token(TokenClass.WHILE, line, column);
                    }
                }
            }


            while (Character.isLetterOrDigit(scanner.peek()) || scanner.peek() == '_'){
                value += scanner.next();
            }
            column = scanner.getColumn();
            line = scanner.getLine();
            return new Token(TokenClass.IDENTIFIER, value, line, column);



        }

        // ... to be completed


        // if we reach this point, it means we did not recognise a valid token
        error(c, line, column);
        return new Token(TokenClass.INVALID, line, column);
    }


}
