package ast;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class PointerType implements Type {
    public final Type type;

    public PointerType(Type type) {
        this.type = type;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitPointerType(this);
    }
}
