#define DEBUG_TYPE "opCounter"
#include "llvm/Transforms/Utils/Local.h"

using namespace llvm;
using namespace std;

namespace {
   struct SimpleDCE : public FunctionPass {
     static char ID;
     SimpleDCE() : FunctionPass(ID) {}
     
     
     virtual bool runOnFunction(Function &F) {
       errs() << "BEFORE DCE\n";
         
         // COUNTOP
         
         
         std::map<std::string, int> opCounter;
         //errs() << "Function " << F.getName() << '\n';
         for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
             for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                 if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                     opCounter[i->getOpcodeName()] = 1;
                 } else {
                     opCounter[i->getOpcodeName()] += 1;
                 }
             }
         }
         std::map <std::string, int>::iterator i1 = opCounter.begin();
         std::map <std::string, int>::iterator e1 = opCounter.end();
         while (i1 != e1) {
             errs() << i1->first << ": " << i1->second << "\n";
             i1++;
         }
         //errs() << "\n";
         opCounter.clear();
       
       errs() << "DCE START\n";
         
         // Append all the instuctions to one list
         std::vector<Instruction*> List;
         for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
             for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                 List.push_back(&*i);
             }
         }
         
         // Loop over all instructions
         
         while (!List.empty()) {
             Instruction *i = List.back();
             List.pop_back();
             
             if (isInstructionTriviallyDead(i)) {
                 
                 // loop over values that the instruction uses and add them to the list
                 for (User::op_iterator o = i->op_begin(), e = i->op_end(); o != e; ++o)
                     if (Instruction *UsedInst = dyn_cast<Instruction>(*o))
                         List.push_back(UsedInst);
                 
                 // erase instructions parent
                 i->eraseFromParent();
                 
                 // remove instruction from list
                 for (std::vector<Instruction*>::iterator l = List.begin();
                      l != List.end(); ) {
                     if (*l == i)
                         l = List.erase(l);
                     else
                         ++l;
                 }
                 
             }
         }
         
         // COUNTOP
         
         //errs() << "Function " << F.getName() << '\n';
         for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
             for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                 if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                     opCounter[i->getOpcodeName()] = 1;
                 } else {
                     opCounter[i->getOpcodeName()] += 1;
                 }
             }
         }
         std::map <std::string, int>::iterator i2 = opCounter.begin();
         std::map <std::string, int>::iterator e2 = opCounter.end();
         while (i2 != e2) {
             errs() << i2->first << ": " << i2->second << "\n";
             i2++;
         }
         //errs() << "\n";
         opCounter.clear();
         
         
         
         
       errs() << "DCE END\n";
       return false;
     }
   };
}
char SimpleDCE::ID = 0;
static RegisterPass<SimpleDCE> X("simpledce", "Simple dead code elimination");

