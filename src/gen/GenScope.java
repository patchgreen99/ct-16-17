package gen;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class GenScope {
    public GenScope outer;
    public Integer fp_offset;
    public Map<String, Offset> symbolTable = new HashMap <String, Offset>();
    public Integer gp_offset = 0;

    public GenScope(GenScope outer) {
        this.fp_offset = outer.fp_offset;
        this.outer = outer;
    }

    public GenScope() {
        this.fp_offset = 0;
    }

    // All scopes above
    public Offset lookup(String name) {
        Offset o = symbolTable.get(name);
        // To be completed...

        if (o == null && outer != null){
            return outer.lookup(name);
        }
        return o;
    }


    // Only the current scope
    public Offset lookupCurrent(String name) {
        // To be completed...
        return symbolTable.get(name);
    }


    public Integer put(Boolean ispointer, String name, PrintWriter writer, Register input) {
        // Make sure its not global
        Offset o = symbolTable.get(name);
        if (o != null) {
            if (o.isglobal) {
                return this.fp_offset;
            }
        }

        this.fp_offset -= 4; // move down the stack
        if (input != null) {
            writer.println("sw " + input + ", " + this.fp_offset + "(" + Register.fp + ")");
        }
        symbolTable.put(name, new Offset(this.fp_offset, false,ispointer));
        return this.fp_offset;
    }

    public Void putglobal(Boolean ispointer, String name, PrintWriter writer, Register input){
        this.gp_offset -= 4;
        writer.println("sw " + input + ", " + this.gp_offset + "(" + Register.gp + ")"); // copy param value to stack
        symbolTable.put(name, new Offset(this.gp_offset, true, ispointer));
        return null;
    }

}
