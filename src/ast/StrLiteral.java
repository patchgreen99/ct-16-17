package ast;

/**
 * Created by patrick.green on 21/10/2016.
 */
public class StrLiteral extends Expr{
    public final String StrLiteral;

    public StrLiteral(String StrLiteral){
        this.StrLiteral = StrLiteral;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitStrLiteral(this);}
}
