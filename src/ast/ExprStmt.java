package ast;


/**
 * Created by patrick.green on 22/10/2016.
 */
public class ExprStmt extends Stmt {
    public final Expr expr;

    public ExprStmt(Expr expr) {
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitExprStmt(this);
    }
}
