#include "minic-stdlib.h"

struct test {
  int j;
  char c;
};

struct thing {
    struct test k;
};

void fibs(){
    thing.k.j = 7;
}

void main(){
    *thing.k = test;
    thing.k.j = 5;
    fibs();
    print_i(thing.k.j);
}
