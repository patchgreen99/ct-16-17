package ast;

/**
 * Created by patrick.green on 17/10/2016.
 */
public class BinOp extends Expr{
    public final Expr expr1;
    public final Expr expr2;
    public final Op op;

    public BinOp(Expr expr1, Op op, Expr expr2){
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.op = op;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitBinOp(this);
    }
}
