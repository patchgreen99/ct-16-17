package sem;

import ast.VarDecl;

/**
 * Created by patrick.green on 25/10/2016.
 */
public class PointerSymbol extends Symbol {
    public VarDecl vardecl;
    public String name;

    public PointerSymbol(VarDecl var){
        this.vardecl = var;
        this.name = var.varName;
    }

}

