package ast;
import java.util.List;

public class VarExpr extends Expr {
    public final String name;
    public VarDecl declaration; // to be filled in by the name analyser
    public StructType structdeclaration;

    public String struct2return;

    public VarExpr(String name){
	this.name = name;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitVarExpr(this);}
}
