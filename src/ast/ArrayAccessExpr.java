package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class ArrayAccessExpr extends Expr{
    public final Expr expr1;
    public final Expr expr2;
    public VarDecl declaration;

    public String struct2return;

    public ArrayAccessExpr(Expr expr1, Expr expr2){
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitArrayAccessExpr(this);}
}
