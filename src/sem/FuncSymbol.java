package sem;

import ast.FunDecl;

/**
 * Created by patrick.green on 23/10/2016.
 */
public class FuncSymbol extends Symbol {
    public FunDecl funcdecl;
    public String name;

    FuncSymbol(FunDecl func){
        this.funcdecl = func;
        this.name = func.name;
    }

}
