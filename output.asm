#program
.data
str0: .asciiz "\n"
str1: .asciiz "     1   2   3\n"
str2: .asciiz "   +---+---+---+\n"
str3: .asciiz "a  | "
str4: .asciiz " | "
str5: .asciiz " | "
str6: .asciiz " |\n"
str7: .asciiz "   +---+---+---+\n"
str8: .asciiz "b  | "
str9: .asciiz " | "
str10: .asciiz " | "
str11: .asciiz " |\n"
str12: .asciiz "   +---+---+---+\n"
str13: .asciiz "c  | "
str14: .asciiz " | "
str15: .asciiz " | "
str16: .asciiz " |\n"
str17: .asciiz "   +---+---+---+\n"
str18: .asciiz "\n"
str19: .asciiz "Player "
str20: .asciiz " has won!\n"
str21: .asciiz "Player "
str22: .asciiz " select move (e.g. a2)>"
str23: .asciiz "That is not a valid move!\n"
str24: .asciiz "That move is not possible!\n"
str25: .asciiz "It's a draw!\n"
str26: .asciiz "Play again? (y/n)> "
INT: .word
CHAR: .word
.text
la $t8, CHAR
sw $t8, -4($gp)
la $t8, CHAR
sw $t8, -8($gp)
la $t8, CHAR
sw $t8, -12($gp)
la $t8, CHAR
sw $t8, -16($gp)
la $t8, CHAR
sw $t8, -20($gp)
la $t8, CHAR
sw $t8, -24($gp)
la $t8, CHAR
sw $t8, -28($gp)
la $t8, CHAR
sw $t8, -32($gp)
la $t8, CHAR
sw $t8, -36($gp)
la $t8, CHAR
sw $t8, -40($gp)
jal main
end: 
li $v0, 10
syscall
#fundecl

reset:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 0
#block
#assign
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -4($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
sw $s7, 0($t9)
#
#assign
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -8($gp)
#
la $t9, 0($s7)
lw $t8, 0($s7)
sw $s6, 0($t9)
#
#assign
#varexpr
la, $s6, -40($gp)
#
la $t9, 0($s6)
lw $t8, 0($s6)
#varexpr
la, $s6, -12($gp)
#
la $t9, 0($s6)
lw $s7, 0($s6)
sw $t8, 0($t9)
#
#assign
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -16($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
sw $s7, 0($t9)
#
#assign
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -20($gp)
#
la $t9, 0($s7)
lw $t8, 0($s7)
sw $s6, 0($t9)
#
#assign
#varexpr
la, $s6, -40($gp)
#
la $t9, 0($s6)
lw $t8, 0($s6)
#varexpr
la, $s6, -24($gp)
#
la $t9, 0($s6)
lw $s7, 0($s6)
sw $t8, 0($t9)
#
#assign
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -28($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
sw $s7, 0($t9)
#
#assign
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -32($gp)
#
la $t9, 0($s7)
lw $t8, 0($s7)
sw $s6, 0($t9)
#
#assign
#varexpr
la, $s6, -40($gp)
#
la $t9, 0($s6)
lw $t8, 0($s6)
#varexpr
la, $s6, -36($gp)
#
la $t9, 0($s6)
lw $s7, 0($s6)
sw $t8, 0($t9)
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
#fundecl

full:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 52
#block
#vardecl
#
#assign
#intliteral
li $t8, 0
#
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
sw $t8, 0($t9)
#
#if
#binop
#varexpr
la, $t8, -4($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
bne $s6, $s7, booltrue0
li, $t8, 0
b, boolfalse0
booltrue0 : 
li, $t8, 1
boolfalse0 :
#
beqz $t8, iffalse0
#assign
#binop
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#intliteral
li $s7, 1
#
add $s5, $s6, $s7
#
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
sw $s5, 0($t9)
#
b iftrue0
iffalse0 :
iftrue0 :
#
#if
#binop
#varexpr
la, $t8, -16($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
bne $s5, $s6, booltrue1
li, $t8, 0
b, boolfalse1
booltrue1 : 
li, $t8, 1
boolfalse1 :
#
beqz $t8, iffalse1
#assign
#binop
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
#intliteral
li $s6, 1
#
add $s7, $s5, $s6
#
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
sw $s7, 0($t9)
#
b iftrue1
iffalse1 :
iftrue1 :
#
#if
#binop
#varexpr
la, $t8, -28($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
bne $s7, $s5, booltrue2
li, $t8, 0
b, boolfalse2
booltrue2 : 
li, $t8, 1
boolfalse2 :
#
beqz $t8, iffalse2
#assign
#binop
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
#intliteral
li $s5, 1
#
add $s6, $s7, $s5
#
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
sw $s6, 0($t9)
#
b iftrue2
iffalse2 :
iftrue2 :
#
#if
#binop
#varexpr
la, $t8, -8($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
bne $s6, $s7, booltrue3
li, $t8, 0
b, boolfalse3
booltrue3 : 
li, $t8, 1
boolfalse3 :
#
beqz $t8, iffalse3
#assign
#binop
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#intliteral
li $s7, 1
#
add $s5, $s6, $s7
#
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
sw $s5, 0($t9)
#
b iftrue3
iffalse3 :
iftrue3 :
#
#if
#binop
#varexpr
la, $t8, -20($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
bne $s5, $s6, booltrue4
li, $t8, 0
b, boolfalse4
booltrue4 : 
li, $t8, 1
boolfalse4 :
#
beqz $t8, iffalse4
#assign
#binop
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
#intliteral
li $s6, 1
#
add $s7, $s5, $s6
#
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
sw $s7, 0($t9)
#
b iftrue4
iffalse4 :
iftrue4 :
#
#if
#binop
#varexpr
la, $t8, -32($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
bne $s7, $s5, booltrue5
li, $t8, 0
b, boolfalse5
booltrue5 : 
li, $t8, 1
boolfalse5 :
#
beqz $t8, iffalse5
#assign
#binop
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
#intliteral
li $s5, 1
#
add $s6, $s7, $s5
#
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
sw $s6, 0($t9)
#
b iftrue5
iffalse5 :
iftrue5 :
#
#if
#binop
#varexpr
la, $t8, -12($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
bne $s6, $s7, booltrue6
li, $t8, 0
b, boolfalse6
booltrue6 : 
li, $t8, 1
boolfalse6 :
#
beqz $t8, iffalse6
#assign
#binop
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#intliteral
li $s7, 1
#
add $s5, $s6, $s7
#
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
sw $s5, 0($t9)
#
b iftrue6
iffalse6 :
iftrue6 :
#
#if
#binop
#varexpr
la, $t8, -24($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
bne $s5, $s6, booltrue7
li, $t8, 0
b, boolfalse7
booltrue7 : 
li, $t8, 1
boolfalse7 :
#
beqz $t8, iffalse7
#assign
#binop
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
#intliteral
li $s6, 1
#
add $s7, $s5, $s6
#
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
sw $s7, 0($t9)
#
b iftrue7
iffalse7 :
iftrue7 :
#
#if
#binop
#varexpr
la, $t8, -36($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -40($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
bne $s7, $s5, booltrue8
li, $t8, 0
b, boolfalse8
booltrue8 : 
li, $t8, 1
boolfalse8 :
#
beqz $t8, iffalse8
#assign
#binop
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
#intliteral
li $s5, 1
#
add $s6, $s7, $s5
#
#varexpr
la, $s5, -8($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
sw $s6, 0($t9)
#
b iftrue8
iffalse8 :
iftrue8 :
#
#if
#binop
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
#intliteral
li $t8, 9
#
beq $s6, $t8, booltrue9
li, $s7, 0
b, boolfalse9
booltrue9 : 
li, $s7, 1
boolfalse9 :
#
beqz $s7, iffalse9
#return
#intliteral
li $t8, 1
#
move $v0, $t8
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
b iftrue9
iffalse9 :
#return
#intliteral
li $t8, 0
#
move $v0, $t8
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
iftrue9 :
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
#fundecl

set:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 108
#block
lw $s7, 4($fp)
sw $s7, -8($fp)
lw $s7, 8($fp)
sw $s7, -12($fp)
lw $s7, 12($fp)
sw $s7, -16($fp)
#vardecl
#
#assign
#intliteral
li $s7, 1
#
#varexpr
la, $t8, -20($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
sw $s7, 0($t9)
#
#if
#binop
#varexpr
la, $s7, -16($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#charliteral
li $s7, 'a'
#
beq $s6, $s7, booltrue10
li, $t8, 0
b, boolfalse10
booltrue10 : 
li, $t8, 1
boolfalse10 :
#
beqz $t8, iffalse10
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#intliteral
li $s7, 1
#
beq $s6, $s7, booltrue11
li, $s5, 0
b, boolfalse11
booltrue11 : 
li, $s5, 1
boolfalse11 :
#
beqz $s5, iffalse11
#block
#if
#binop
#varexpr
la, $s7, -4($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s4, 0($s7)
beq $s6, $s4, booltrue12
li, $s7, 0
b, boolfalse12
booltrue12 : 
li, $s7, 1
boolfalse12 :
#
beqz $s7, iffalse12
#assign
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s6, 0($s4)
#varexpr
la, $s4, -4($gp)
#
la $t9, 0($s4)
lw $s3, 0($s4)
sw $s6, 0($t9)
#
b iftrue12
iffalse12 :
#assign
#binop
#intliteral
li $s6, 0
#
#intliteral
li $s3, 1
#
sub $s4, $s6, $s3
#
#varexpr
la, $s3, -20($fp)
#
la $t9, 0($s3)
lw $s6, 0($s3)
sw $s4, 0($t9)
#
iftrue12 :
#
#
b iftrue11
iffalse11 :
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s4, 0($s7)
#intliteral
li $s7, 2
#
beq $s4, $s7, booltrue13
li, $s6, 0
b, boolfalse13
booltrue13 : 
li, $s6, 1
boolfalse13 :
#
beqz $s6, iffalse13
#block
#if
#binop
#varexpr
la, $s7, -8($gp)
#
la $t9, 0($s7)
lw $s4, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s3, 0($s7)
beq $s4, $s3, booltrue14
li, $s7, 0
b, boolfalse14
booltrue14 : 
li, $s7, 1
boolfalse14 :
#
beqz $s7, iffalse14
#assign
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s4, 0($s3)
#varexpr
la, $s3, -8($gp)
#
la $t9, 0($s3)
lw $s2, 0($s3)
sw $s4, 0($t9)
#
b iftrue14
iffalse14 :
#assign
#binop
#intliteral
li $s4, 0
#
#intliteral
li $s2, 1
#
sub $s3, $s4, $s2
#
#varexpr
la, $s2, -20($fp)
#
la $t9, 0($s2)
lw $s4, 0($s2)
sw $s3, 0($t9)
#
iftrue14 :
#
#
b iftrue13
iffalse13 :
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s3, 0($s7)
#intliteral
li $s7, 3
#
beq $s3, $s7, booltrue15
li, $s4, 0
b, boolfalse15
booltrue15 : 
li, $s4, 1
boolfalse15 :
#
beqz $s4, iffalse15
#block
#if
#binop
#varexpr
la, $s7, -12($gp)
#
la $t9, 0($s7)
lw $s3, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s2, 0($s7)
beq $s3, $s2, booltrue16
li, $s7, 0
b, boolfalse16
booltrue16 : 
li, $s7, 1
boolfalse16 :
#
beqz $s7, iffalse16
#assign
#varexpr
la, $s2, -8($fp)
#
la $t9, 0($s2)
lw $s3, 0($s2)
#varexpr
la, $s2, -12($gp)
#
la $t9, 0($s2)
lw $s1, 0($s2)
sw $s3, 0($t9)
#
b iftrue16
iffalse16 :
#assign
#binop
#intliteral
li $s3, 0
#
#intliteral
li $s1, 1
#
sub $s2, $s3, $s1
#
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s3, 0($s1)
sw $s2, 0($t9)
#
iftrue16 :
#
#
b iftrue15
iffalse15 :
#block
#assign
#intliteral
li $s7, 0
#
#varexpr
la, $s2, -20($fp)
#
la $t9, 0($s2)
lw $s3, 0($s2)
sw $s7, 0($t9)
#
#
iftrue15 :
#
#
iftrue13 :
#
#
iftrue11 :
#
#
b iftrue10
iffalse10 :
#block
#if
#binop
#varexpr
la, $s5, -16($fp)
#
la $t9, 0($s5)
lw $s6, 0($s5)
#charliteral
li $s5, 'b'
#
beq $s6, $s5, booltrue17
li, $s4, 0
b, boolfalse17
booltrue17 : 
li, $s4, 1
boolfalse17 :
#
beqz $s4, iffalse17
#block
#if
#binop
#varexpr
la, $s5, -12($fp)
#
la $t9, 0($s5)
lw $s6, 0($s5)
#intliteral
li $s5, 1
#
beq $s6, $s5, booltrue18
li, $s7, 0
b, boolfalse18
booltrue18 : 
li, $s7, 1
boolfalse18 :
#
beqz $s7, iffalse18
#block
#if
#binop
#varexpr
la, $s5, -16($gp)
#
la $t9, 0($s5)
lw $s6, 0($s5)
#varexpr
la, $s5, -40($gp)
#
la $t9, 0($s5)
lw $s3, 0($s5)
beq $s6, $s3, booltrue19
li, $s5, 0
b, boolfalse19
booltrue19 : 
li, $s5, 1
boolfalse19 :
#
beqz $s5, iffalse19
#assign
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s6, 0($s3)
#varexpr
la, $s3, -16($gp)
#
la $t9, 0($s3)
lw $s2, 0($s3)
sw $s6, 0($t9)
#
b iftrue19
iffalse19 :
#assign
#binop
#intliteral
li $s6, 0
#
#intliteral
li $s2, 1
#
sub $s3, $s6, $s2
#
#varexpr
la, $s2, -20($fp)
#
la $t9, 0($s2)
lw $s6, 0($s2)
sw $s3, 0($t9)
#
iftrue19 :
#
#
b iftrue18
iffalse18 :
#block
#if
#binop
#varexpr
la, $s5, -12($fp)
#
la $t9, 0($s5)
lw $s3, 0($s5)
#intliteral
li $s5, 2
#
beq $s3, $s5, booltrue20
li, $s6, 0
b, boolfalse20
booltrue20 : 
li, $s6, 1
boolfalse20 :
#
beqz $s6, iffalse20
#block
#if
#binop
#varexpr
la, $s5, -20($gp)
#
la $t9, 0($s5)
lw $s3, 0($s5)
#varexpr
la, $s5, -40($gp)
#
la $t9, 0($s5)
lw $s2, 0($s5)
beq $s3, $s2, booltrue21
li, $s5, 0
b, boolfalse21
booltrue21 : 
li, $s5, 1
boolfalse21 :
#
beqz $s5, iffalse21
#assign
#varexpr
la, $s2, -8($fp)
#
la $t9, 0($s2)
lw $s3, 0($s2)
#varexpr
la, $s2, -20($gp)
#
la $t9, 0($s2)
lw $s1, 0($s2)
sw $s3, 0($t9)
#
b iftrue21
iffalse21 :
#assign
#binop
#intliteral
li $s3, 0
#
#intliteral
li $s1, 1
#
sub $s2, $s3, $s1
#
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s3, 0($s1)
sw $s2, 0($t9)
#
iftrue21 :
#
#
b iftrue20
iffalse20 :
#block
#if
#binop
#varexpr
la, $s5, -12($fp)
#
la $t9, 0($s5)
lw $s2, 0($s5)
#intliteral
li $s5, 3
#
beq $s2, $s5, booltrue22
li, $s3, 0
b, boolfalse22
booltrue22 : 
li, $s3, 1
boolfalse22 :
#
beqz $s3, iffalse22
#block
#if
#binop
#varexpr
la, $s5, -24($gp)
#
la $t9, 0($s5)
lw $s2, 0($s5)
#varexpr
la, $s5, -40($gp)
#
la $t9, 0($s5)
lw $s1, 0($s5)
beq $s2, $s1, booltrue23
li, $s5, 0
b, boolfalse23
booltrue23 : 
li, $s5, 1
boolfalse23 :
#
beqz $s5, iffalse23
#assign
#varexpr
la, $s1, -8($fp)
#
la $t9, 0($s1)
lw $s2, 0($s1)
#varexpr
la, $s1, -24($gp)
#
la $t9, 0($s1)
lw $s0, 0($s1)
sw $s2, 0($t9)
#
b iftrue23
iffalse23 :
#assign
#binop
#intliteral
li $s2, 0
#
#intliteral
li $s0, 1
#
sub $s1, $s2, $s0
#
#varexpr
la, $s0, -20($fp)
#
la $t9, 0($s0)
lw $s2, 0($s0)
sw $s1, 0($t9)
#
iftrue23 :
#
#
b iftrue22
iffalse22 :
#block
#assign
#intliteral
li $s5, 0
#
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s2, 0($s1)
sw $s5, 0($t9)
#
#
iftrue22 :
#
#
iftrue20 :
#
#
iftrue18 :
#
#
b iftrue17
iffalse17 :
#block
#if
#binop
#varexpr
la, $s7, -16($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#charliteral
li $s7, 'c'
#
beq $s6, $s7, booltrue24
li, $s3, 0
b, boolfalse24
booltrue24 : 
li, $s3, 1
boolfalse24 :
#
beqz $s3, iffalse24
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#intliteral
li $s7, 1
#
beq $s6, $s7, booltrue25
li, $s5, 0
b, boolfalse25
booltrue25 : 
li, $s5, 1
boolfalse25 :
#
beqz $s5, iffalse25
#block
#if
#binop
#varexpr
la, $s7, -28($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s2, 0($s7)
beq $s6, $s2, booltrue26
li, $s7, 0
b, boolfalse26
booltrue26 : 
li, $s7, 1
boolfalse26 :
#
beqz $s7, iffalse26
#assign
#varexpr
la, $s2, -8($fp)
#
la $t9, 0($s2)
lw $s6, 0($s2)
#varexpr
la, $s2, -28($gp)
#
la $t9, 0($s2)
lw $s1, 0($s2)
sw $s6, 0($t9)
#
b iftrue26
iffalse26 :
#assign
#binop
#intliteral
li $s6, 0
#
#intliteral
li $s1, 1
#
sub $s2, $s6, $s1
#
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s6, 0($s1)
sw $s2, 0($t9)
#
iftrue26 :
#
#
b iftrue25
iffalse25 :
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s2, 0($s7)
#intliteral
li $s7, 2
#
beq $s2, $s7, booltrue27
li, $s6, 0
b, boolfalse27
booltrue27 : 
li, $s6, 1
boolfalse27 :
#
beqz $s6, iffalse27
#block
#if
#binop
#varexpr
la, $s7, -32($gp)
#
la $t9, 0($s7)
lw $s2, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s1, 0($s7)
beq $s2, $s1, booltrue28
li, $s7, 0
b, boolfalse28
booltrue28 : 
li, $s7, 1
boolfalse28 :
#
beqz $s7, iffalse28
#assign
#varexpr
la, $s1, -8($fp)
#
la $t9, 0($s1)
lw $s2, 0($s1)
#varexpr
la, $s1, -32($gp)
#
la $t9, 0($s1)
lw $s0, 0($s1)
sw $s2, 0($t9)
#
b iftrue28
iffalse28 :
#assign
#binop
#intliteral
li $s2, 0
#
#intliteral
li $s0, 1
#
sub $s1, $s2, $s0
#
#varexpr
la, $s0, -20($fp)
#
la $t9, 0($s0)
lw $s2, 0($s0)
sw $s1, 0($t9)
#
iftrue28 :
#
#
b iftrue27
iffalse27 :
#block
#if
#binop
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s1, 0($s7)
#intliteral
li $s7, 3
#
beq $s1, $s7, booltrue29
li, $s2, 0
b, boolfalse29
booltrue29 : 
li, $s2, 1
boolfalse29 :
#
beqz $s2, iffalse29
#block
#if
#binop
#varexpr
la, $s7, -36($gp)
#
la $t9, 0($s7)
lw $s1, 0($s7)
#varexpr
la, $s7, -40($gp)
#
la $t9, 0($s7)
lw $s0, 0($s7)
beq $s1, $s0, booltrue30
li, $s7, 0
b, boolfalse30
booltrue30 : 
li, $s7, 1
boolfalse30 :
#
beqz $s7, iffalse30
#assign
#varexpr
la, $s0, -8($fp)
#
la $t9, 0($s0)
lw $s1, 0($s0)
#varexpr
la, $s0, -36($gp)
#
la $t9, 0($s0)
lw $t7, 0($s0)
sw $s1, 0($t9)
#
b iftrue30
iffalse30 :
#assign
#binop
#intliteral
li $s1, 0
#
#intliteral
li $t7, 1
#
sub $s0, $s1, $t7
#
#varexpr
la, $t7, -20($fp)
#
la $t9, 0($t7)
lw $s1, 0($t7)
sw $s0, 0($t9)
#
iftrue30 :
#
#
b iftrue29
iffalse29 :
#block
#assign
#intliteral
li $s7, 0
#
#varexpr
la, $s0, -20($fp)
#
la $t9, 0($s0)
lw $s1, 0($s0)
sw $s7, 0($t9)
#
#
iftrue29 :
#
#
iftrue27 :
#
#
iftrue25 :
#
#
b iftrue24
iffalse24 :
#block
#assign
#intliteral
li $s5, 0
#
#varexpr
la, $s6, -20($fp)
#
la $t9, 0($s6)
lw $s2, 0($s6)
sw $s5, 0($t9)
#
#
iftrue24 :
#
#
iftrue17 :
#
#
iftrue10 :
#
#return
#varexpr
la, $t8, -20($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
move $v0, $s4
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 12
#
#fundecl

printGame:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 0
#block
#exprstmt
#funcallexpr
#strliteral
la $s4, str0
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str1
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str2
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str3
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -8($fp)
sw $t4, -12($fp)
sw $t2, -16($fp)
sw $s2, -20($fp)
sw $t8, -24($fp)
sw $s4, -28($fp)
sw $t6, -32($fp)
sw $s7, -36($fp)
sw $s5, -40($fp)
sw $t1, -44($fp)
sw $s0, -48($fp)
sw $s6, -52($fp)
sw $t0, -56($fp)
sw $s1, -60($fp)
sw $s3, -64($fp)
sw $t7, -68($fp)
sw $t5, -72($fp)
sw $t9, -76($fp)
#varexpr
la, $s4, -4($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal print_c
lw $t9, -76($fp)
lw $t5, -72($fp)
lw $t7, -68($fp)
lw $s3, -64($fp)
lw $s1, -60($fp)
lw $t0, -56($fp)
lw $s6, -52($fp)
lw $s0, -48($fp)
lw $t1, -44($fp)
lw $s5, -40($fp)
lw $s7, -36($fp)
lw $t6, -32($fp)
lw $s4, -28($fp)
lw $t8, -24($fp)
lw $s2, -20($fp)
lw $t2, -16($fp)
lw $t4, -12($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str4
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -80($fp)
sw $t4, -84($fp)
sw $t2, -88($fp)
sw $s2, -92($fp)
sw $t8, -96($fp)
sw $s4, -100($fp)
sw $t6, -104($fp)
sw $s7, -108($fp)
sw $s5, -112($fp)
sw $t1, -116($fp)
sw $s0, -120($fp)
sw $s6, -124($fp)
sw $t0, -128($fp)
sw $s1, -132($fp)
sw $s3, -136($fp)
sw $t7, -140($fp)
sw $t5, -144($fp)
sw $t9, -148($fp)
#varexpr
la, $t8, -8($gp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal print_c
lw $t9, -148($fp)
lw $t5, -144($fp)
lw $t7, -140($fp)
lw $s3, -136($fp)
lw $s1, -132($fp)
lw $t0, -128($fp)
lw $s6, -124($fp)
lw $s0, -120($fp)
lw $t1, -116($fp)
lw $s5, -112($fp)
lw $s7, -108($fp)
lw $t6, -104($fp)
lw $s4, -100($fp)
lw $t8, -96($fp)
lw $s2, -92($fp)
lw $t2, -88($fp)
lw $t4, -84($fp)
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str5
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -152($fp)
sw $t4, -156($fp)
sw $t2, -160($fp)
sw $s2, -164($fp)
sw $t8, -168($fp)
sw $s4, -172($fp)
sw $t6, -176($fp)
sw $s7, -180($fp)
sw $s5, -184($fp)
sw $t1, -188($fp)
sw $s0, -192($fp)
sw $s6, -196($fp)
sw $t0, -200($fp)
sw $s1, -204($fp)
sw $s3, -208($fp)
sw $t7, -212($fp)
sw $t5, -216($fp)
sw $t9, -220($fp)
#varexpr
la, $s4, -12($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal print_c
lw $t9, -220($fp)
lw $t5, -216($fp)
lw $t7, -212($fp)
lw $s3, -208($fp)
lw $s1, -204($fp)
lw $t0, -200($fp)
lw $s6, -196($fp)
lw $s0, -192($fp)
lw $t1, -188($fp)
lw $s5, -184($fp)
lw $s7, -180($fp)
lw $t6, -176($fp)
lw $s4, -172($fp)
lw $t8, -168($fp)
lw $s2, -164($fp)
lw $t2, -160($fp)
lw $t4, -156($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str6
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str7
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str8
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -224($fp)
sw $t4, -228($fp)
sw $t2, -232($fp)
sw $s2, -236($fp)
sw $t8, -240($fp)
sw $s4, -244($fp)
sw $t6, -248($fp)
sw $s7, -252($fp)
sw $s5, -256($fp)
sw $t1, -260($fp)
sw $s0, -264($fp)
sw $s6, -268($fp)
sw $t0, -272($fp)
sw $s1, -276($fp)
sw $s3, -280($fp)
sw $t7, -284($fp)
sw $t5, -288($fp)
sw $t9, -292($fp)
#varexpr
la, $t8, -16($gp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal print_c
lw $t9, -292($fp)
lw $t5, -288($fp)
lw $t7, -284($fp)
lw $s3, -280($fp)
lw $s1, -276($fp)
lw $t0, -272($fp)
lw $s6, -268($fp)
lw $s0, -264($fp)
lw $t1, -260($fp)
lw $s5, -256($fp)
lw $s7, -252($fp)
lw $t6, -248($fp)
lw $s4, -244($fp)
lw $t8, -240($fp)
lw $s2, -236($fp)
lw $t2, -232($fp)
lw $t4, -228($fp)
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str9
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -296($fp)
sw $t4, -300($fp)
sw $t2, -304($fp)
sw $s2, -308($fp)
sw $t8, -312($fp)
sw $s4, -316($fp)
sw $t6, -320($fp)
sw $s7, -324($fp)
sw $s5, -328($fp)
sw $t1, -332($fp)
sw $s0, -336($fp)
sw $s6, -340($fp)
sw $t0, -344($fp)
sw $s1, -348($fp)
sw $s3, -352($fp)
sw $t7, -356($fp)
sw $t5, -360($fp)
sw $t9, -364($fp)
#varexpr
la, $s4, -20($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal print_c
lw $t9, -364($fp)
lw $t5, -360($fp)
lw $t7, -356($fp)
lw $s3, -352($fp)
lw $s1, -348($fp)
lw $t0, -344($fp)
lw $s6, -340($fp)
lw $s0, -336($fp)
lw $t1, -332($fp)
lw $s5, -328($fp)
lw $s7, -324($fp)
lw $t6, -320($fp)
lw $s4, -316($fp)
lw $t8, -312($fp)
lw $s2, -308($fp)
lw $t2, -304($fp)
lw $t4, -300($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str10
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -368($fp)
sw $t4, -372($fp)
sw $t2, -376($fp)
sw $s2, -380($fp)
sw $t8, -384($fp)
sw $s4, -388($fp)
sw $t6, -392($fp)
sw $s7, -396($fp)
sw $s5, -400($fp)
sw $t1, -404($fp)
sw $s0, -408($fp)
sw $s6, -412($fp)
sw $t0, -416($fp)
sw $s1, -420($fp)
sw $s3, -424($fp)
sw $t7, -428($fp)
sw $t5, -432($fp)
sw $t9, -436($fp)
#varexpr
la, $t8, -24($gp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal print_c
lw $t9, -436($fp)
lw $t5, -432($fp)
lw $t7, -428($fp)
lw $s3, -424($fp)
lw $s1, -420($fp)
lw $t0, -416($fp)
lw $s6, -412($fp)
lw $s0, -408($fp)
lw $t1, -404($fp)
lw $s5, -400($fp)
lw $s7, -396($fp)
lw $t6, -392($fp)
lw $s4, -388($fp)
lw $t8, -384($fp)
lw $s2, -380($fp)
lw $t2, -376($fp)
lw $t4, -372($fp)
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str11
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str12
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str13
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -440($fp)
sw $t4, -444($fp)
sw $t2, -448($fp)
sw $s2, -452($fp)
sw $t8, -456($fp)
sw $s4, -460($fp)
sw $t6, -464($fp)
sw $s7, -468($fp)
sw $s5, -472($fp)
sw $t1, -476($fp)
sw $s0, -480($fp)
sw $s6, -484($fp)
sw $t0, -488($fp)
sw $s1, -492($fp)
sw $s3, -496($fp)
sw $t7, -500($fp)
sw $t5, -504($fp)
sw $t9, -508($fp)
#varexpr
la, $s4, -28($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal print_c
lw $t9, -508($fp)
lw $t5, -504($fp)
lw $t7, -500($fp)
lw $s3, -496($fp)
lw $s1, -492($fp)
lw $t0, -488($fp)
lw $s6, -484($fp)
lw $s0, -480($fp)
lw $t1, -476($fp)
lw $s5, -472($fp)
lw $s7, -468($fp)
lw $t6, -464($fp)
lw $s4, -460($fp)
lw $t8, -456($fp)
lw $s2, -452($fp)
lw $t2, -448($fp)
lw $t4, -444($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str14
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -512($fp)
sw $t4, -516($fp)
sw $t2, -520($fp)
sw $s2, -524($fp)
sw $t8, -528($fp)
sw $s4, -532($fp)
sw $t6, -536($fp)
sw $s7, -540($fp)
sw $s5, -544($fp)
sw $t1, -548($fp)
sw $s0, -552($fp)
sw $s6, -556($fp)
sw $t0, -560($fp)
sw $s1, -564($fp)
sw $s3, -568($fp)
sw $t7, -572($fp)
sw $t5, -576($fp)
sw $t9, -580($fp)
#varexpr
la, $t8, -32($gp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal print_c
lw $t9, -580($fp)
lw $t5, -576($fp)
lw $t7, -572($fp)
lw $s3, -568($fp)
lw $s1, -564($fp)
lw $t0, -560($fp)
lw $s6, -556($fp)
lw $s0, -552($fp)
lw $t1, -548($fp)
lw $s5, -544($fp)
lw $s7, -540($fp)
lw $t6, -536($fp)
lw $s4, -532($fp)
lw $t8, -528($fp)
lw $s2, -524($fp)
lw $t2, -520($fp)
lw $t4, -516($fp)
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str15
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -584($fp)
sw $t4, -588($fp)
sw $t2, -592($fp)
sw $s2, -596($fp)
sw $t8, -600($fp)
sw $s4, -604($fp)
sw $t6, -608($fp)
sw $s7, -612($fp)
sw $s5, -616($fp)
sw $t1, -620($fp)
sw $s0, -624($fp)
sw $s6, -628($fp)
sw $t0, -632($fp)
sw $s1, -636($fp)
sw $s3, -640($fp)
sw $t7, -644($fp)
sw $t5, -648($fp)
sw $t9, -652($fp)
#varexpr
la, $s4, -36($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal print_c
lw $t9, -652($fp)
lw $t5, -648($fp)
lw $t7, -644($fp)
lw $s3, -640($fp)
lw $s1, -636($fp)
lw $t0, -632($fp)
lw $s6, -628($fp)
lw $s0, -624($fp)
lw $t1, -620($fp)
lw $s5, -616($fp)
lw $s7, -612($fp)
lw $t6, -608($fp)
lw $s4, -604($fp)
lw $t8, -600($fp)
lw $s2, -596($fp)
lw $t2, -592($fp)
lw $t4, -588($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str16
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str17
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $t8, str18
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
#fundecl

printWinner:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 4
#block
lw $t8, 4($fp)
sw $t8, -8($fp)
#exprstmt
#funcallexpr
#strliteral
la $t8, str19
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -12($fp)
sw $t4, -16($fp)
sw $t2, -20($fp)
sw $s2, -24($fp)
sw $t8, -28($fp)
sw $s4, -32($fp)
sw $t6, -36($fp)
sw $s7, -40($fp)
sw $s5, -44($fp)
sw $t1, -48($fp)
sw $s0, -52($fp)
sw $s6, -56($fp)
sw $t0, -60($fp)
sw $s1, -64($fp)
sw $s3, -68($fp)
sw $t7, -72($fp)
sw $t5, -76($fp)
sw $t9, -80($fp)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal print_i
lw $t9, -80($fp)
lw $t5, -76($fp)
lw $t7, -72($fp)
lw $s3, -68($fp)
lw $s1, -64($fp)
lw $t0, -60($fp)
lw $s6, -56($fp)
lw $s0, -52($fp)
lw $t1, -48($fp)
lw $s5, -44($fp)
lw $s7, -40($fp)
lw $t6, -36($fp)
lw $s4, -32($fp)
lw $t8, -28($fp)
lw $s2, -24($fp)
lw $t2, -20($fp)
lw $t4, -16($fp)
#
move $s4, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s4, str20
#
move $a0, $s4
jal print_s
#
move $s4, $v0
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4
#
#fundecl

switchPlayer:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 12
#block
lw $s4, 4($fp)
sw $s4, -8($fp)
#if
#binop
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
#intliteral
li $s4, 1
#
beq $t8, $s4, booltrue31
li, $s3, 0
b, boolfalse31
booltrue31 : 
li, $s3, 1
boolfalse31 :
#
beqz $s3, iffalse31
#return
#intliteral
li $s4, 2
#
move $v0, $s4
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
b iftrue31
iffalse31 :
#return
#intliteral
li $s4, 1
#
move $v0, $s4
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
iftrue31 :
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4
#
#fundecl

get_mark:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 8
#block
lw $s3, 4($fp)
sw $s3, -8($fp)
#if
#binop
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s4, 0($s3)
#intliteral
li $s3, 1
#
beq $s4, $s3, booltrue32
li, $t8, 0
b, boolfalse32
booltrue32 : 
li, $t8, 1
boolfalse32 :
#
beqz $t8, iffalse32
#return
#charliteral
li $s3, 'X'
#
move $v0, $s3
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
b iftrue32
iffalse32 :
#return
#charliteral
li $s3, 'O'
#
move $v0, $s3
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
iftrue32 :
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4
#
#fundecl

selectmove:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 16
#block
lw $t8, 4($fp)
sw $t8, -8($fp)
#vardecl
#
#vardecl
#
#vardecl
#
#vardecl
#
#vardecl
#
#assign
#intliteral
li $t8, 1
#
#varexpr
la, $s3, -20($fp)
#
la $t9, 0($s3)
lw $s4, 0($s3)
sw $t8, 0($t9)
#
#while
while0:
#varexpr
la, $t8, -20($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
beqz, $s4, whilefalse0
#block
#exprstmt
#funcallexpr
#strliteral
la $t8, str21
#
move $a0, $t8
jal print_s
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -32($fp)
sw $t4, -36($fp)
sw $t2, -40($fp)
sw $s2, -44($fp)
sw $t8, -48($fp)
sw $s4, -52($fp)
sw $t6, -56($fp)
sw $s7, -60($fp)
sw $s5, -64($fp)
sw $t1, -68($fp)
sw $s0, -72($fp)
sw $s6, -76($fp)
sw $t0, -80($fp)
sw $s1, -84($fp)
sw $s3, -88($fp)
sw $t7, -92($fp)
sw $t5, -96($fp)
sw $t9, -100($fp)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s3, 0($t8)
subu $sp, $sp, 4
sw $s3, 4($sp)
jal print_i
lw $t9, -100($fp)
lw $t5, -96($fp)
lw $t7, -92($fp)
lw $s3, -88($fp)
lw $s1, -84($fp)
lw $t0, -80($fp)
lw $s6, -76($fp)
lw $s0, -72($fp)
lw $t1, -68($fp)
lw $s5, -64($fp)
lw $s7, -60($fp)
lw $t6, -56($fp)
lw $s4, -52($fp)
lw $t8, -48($fp)
lw $s2, -44($fp)
lw $t2, -40($fp)
lw $t4, -36($fp)
#
move $s3, $v0
#
#exprstmt
#funcallexpr
#strliteral
la $s3, str22
#
move $a0, $s3
jal print_s
#
move $s3, $v0
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -104($fp)
sw $t4, -108($fp)
sw $t2, -112($fp)
sw $s2, -116($fp)
sw $t8, -120($fp)
sw $s4, -124($fp)
sw $t6, -128($fp)
sw $s7, -132($fp)
sw $s5, -136($fp)
sw $t1, -140($fp)
sw $s0, -144($fp)
sw $s6, -148($fp)
sw $t0, -152($fp)
sw $s1, -156($fp)
sw $s3, -160($fp)
sw $t7, -164($fp)
sw $t5, -168($fp)
sw $t9, -172($fp)
jal read_c
lw $t9, -172($fp)
lw $t5, -168($fp)
lw $t7, -164($fp)
lw $s3, -160($fp)
lw $s1, -156($fp)
lw $t0, -152($fp)
lw $s6, -148($fp)
lw $s0, -144($fp)
lw $t1, -140($fp)
lw $s5, -136($fp)
lw $s7, -132($fp)
lw $t6, -128($fp)
lw $s4, -124($fp)
lw $t8, -120($fp)
lw $s2, -116($fp)
lw $t2, -112($fp)
lw $t4, -108($fp)
#
move $s3, $v0
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
sw $s3, 0($t9)
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -176($fp)
sw $t4, -180($fp)
sw $t2, -184($fp)
sw $s2, -188($fp)
sw $t8, -192($fp)
sw $s4, -196($fp)
sw $t6, -200($fp)
sw $s7, -204($fp)
sw $s5, -208($fp)
sw $t1, -212($fp)
sw $s0, -216($fp)
sw $s6, -220($fp)
sw $t0, -224($fp)
sw $s1, -228($fp)
sw $s3, -232($fp)
sw $t7, -236($fp)
sw $t5, -240($fp)
sw $t9, -244($fp)
jal read_i
lw $t9, -244($fp)
lw $t5, -240($fp)
lw $t7, -236($fp)
lw $s3, -232($fp)
lw $s1, -228($fp)
lw $t0, -224($fp)
lw $s6, -220($fp)
lw $s0, -216($fp)
lw $t1, -212($fp)
lw $s5, -208($fp)
lw $s7, -204($fp)
lw $t6, -200($fp)
lw $s4, -196($fp)
lw $t8, -192($fp)
lw $s2, -188($fp)
lw $t2, -184($fp)
lw $t4, -180($fp)
#
move $s3, $v0
#varexpr
la, $s5, -16($fp)
#
la $t9, 0($s5)
lw $t8, 0($s5)
sw $s3, 0($t9)
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -248($fp)
sw $t4, -252($fp)
sw $t2, -256($fp)
sw $s2, -260($fp)
sw $t8, -264($fp)
sw $s4, -268($fp)
sw $t6, -272($fp)
sw $s7, -276($fp)
sw $s5, -280($fp)
sw $t1, -284($fp)
sw $s0, -288($fp)
sw $s6, -292($fp)
sw $t0, -296($fp)
sw $s1, -300($fp)
sw $s3, -304($fp)
sw $t7, -308($fp)
sw $t5, -312($fp)
sw $t9, -316($fp)
jal read_c
lw $t9, -316($fp)
lw $t5, -312($fp)
lw $t7, -308($fp)
lw $s3, -304($fp)
lw $s1, -300($fp)
lw $t0, -296($fp)
lw $s6, -292($fp)
lw $s0, -288($fp)
lw $t1, -284($fp)
lw $s5, -280($fp)
lw $s7, -276($fp)
lw $t6, -272($fp)
lw $s4, -268($fp)
lw $t8, -264($fp)
lw $s2, -260($fp)
lw $t2, -256($fp)
lw $t4, -252($fp)
#
move $s3, $v0
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -320($fp)
sw $t4, -324($fp)
sw $t2, -328($fp)
sw $s2, -332($fp)
sw $t8, -336($fp)
sw $s4, -340($fp)
sw $t6, -344($fp)
sw $s7, -348($fp)
sw $s5, -352($fp)
sw $t1, -356($fp)
sw $s0, -360($fp)
sw $s6, -364($fp)
sw $t0, -368($fp)
sw $s1, -372($fp)
sw $s3, -376($fp)
sw $t7, -380($fp)
sw $t5, -384($fp)
sw $t9, -388($fp)
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $t8, 0($s3)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal get_mark
lw $t9, -388($fp)
lw $t5, -384($fp)
lw $t7, -380($fp)
lw $s3, -376($fp)
lw $s1, -372($fp)
lw $t0, -368($fp)
lw $s6, -364($fp)
lw $s0, -360($fp)
lw $t1, -356($fp)
lw $s5, -352($fp)
lw $s7, -348($fp)
lw $t6, -344($fp)
lw $s4, -340($fp)
lw $t8, -336($fp)
lw $s2, -332($fp)
lw $t2, -328($fp)
lw $t4, -324($fp)
#
move $t8, $v0
#varexpr
la, $s3, -28($fp)
#
la $t9, 0($s3)
lw $s5, 0($s3)
sw $t8, 0($t9)
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -392($fp)
sw $t4, -396($fp)
sw $t2, -400($fp)
sw $s2, -404($fp)
sw $t8, -408($fp)
sw $s4, -412($fp)
sw $t6, -416($fp)
sw $s7, -420($fp)
sw $s5, -424($fp)
sw $t1, -428($fp)
sw $s0, -432($fp)
sw $s6, -436($fp)
sw $t0, -440($fp)
sw $s1, -444($fp)
sw $s3, -448($fp)
sw $t7, -452($fp)
sw $t5, -456($fp)
sw $t9, -460($fp)
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
subu $sp, $sp, 4
sw $s5, 4($sp)
#varexpr
la, $s5, -16($fp)
#
la $t9, 0($s5)
lw $t8, 0($s5)
subu $sp, $sp, 4
sw $t8, 4($sp)
#varexpr
la, $t8, -28($fp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
subu $sp, $sp, 4
sw $s5, 4($sp)
jal set
lw $t9, -460($fp)
lw $t5, -456($fp)
lw $t7, -452($fp)
lw $s3, -448($fp)
lw $s1, -444($fp)
lw $t0, -440($fp)
lw $s6, -436($fp)
lw $s0, -432($fp)
lw $t1, -428($fp)
lw $s5, -424($fp)
lw $s7, -420($fp)
lw $t6, -416($fp)
lw $s4, -412($fp)
lw $t8, -408($fp)
lw $s2, -404($fp)
lw $t2, -400($fp)
lw $t4, -396($fp)
#
move $s5, $v0
#varexpr
la, $t8, -24($fp)
#
la $t9, 0($t8)
lw $s3, 0($t8)
sw $s5, 0($t9)
#
#if
#binop
#varexpr
la, $s5, -24($fp)
#
la $t9, 0($s5)
lw $s3, 0($s5)
#intliteral
li $s5, 0
#
beq $s3, $s5, booltrue33
li, $t8, 0
b, boolfalse33
booltrue33 : 
li, $t8, 1
boolfalse33 :
#
beqz $t8, iffalse33
#block
#exprstmt
#funcallexpr
#strliteral
la $s5, str23
#
move $a0, $s5
jal print_s
#
move $s5, $v0
#
#
b iftrue33
iffalse33 :
#block
#if
#binop
#varexpr
la, $s5, -24($fp)
#
la $t9, 0($s5)
lw $s3, 0($s5)
#binop
#intliteral
li $s5, 0
#
#intliteral
li $s2, 1
#
sub $s6, $s5, $s2
#
beq $s3, $s6, booltrue34
li, $s2, 0
b, boolfalse34
booltrue34 : 
li, $s2, 1
boolfalse34 :
#
beqz $s2, iffalse34
#exprstmt
#funcallexpr
#strliteral
la $s6, str24
#
move $a0, $s6
jal print_s
#
move $s6, $v0
#
b iftrue34
iffalse34 :
#assign
#intliteral
li $s6, 0
#
#varexpr
la, $s3, -20($fp)
#
la $t9, 0($s3)
lw $s5, 0($s3)
sw $s6, 0($t9)
#
iftrue34 :
#
#
iftrue33 :
#
#
b while0
whilefalse0:
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4
#
#fundecl

won:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 36
#block
lw $s4, 4($fp)
sw $s4, -8($fp)
#vardecl
#
#assign
#intliteral
li $s4, 0
#
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s2, 0($t8)
sw $s4, 0($t9)
#
#if
#binop
#varexpr
la, $s4, -4($gp)
#
la $t9, 0($s4)
lw $s2, 0($s4)
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
beq $s2, $t8, booltrue35
li, $s4, 0
b, boolfalse35
booltrue35 : 
li, $s4, 1
boolfalse35 :
#
beqz $s4, iffalse35
#block
#if
#binop
#varexpr
la, $t8, -16($gp)
#
la $t9, 0($t8)
lw $s2, 0($t8)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
beq $s2, $s6, booltrue36
li, $t8, 0
b, boolfalse36
booltrue36 : 
li, $t8, 1
boolfalse36 :
#
beqz $t8, iffalse36
#block
#if
#binop
#varexpr
la, $s6, -28($gp)
#
la $t9, 0($s6)
lw $s2, 0($s6)
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
beq $s2, $s5, booltrue37
li, $s6, 0
b, boolfalse37
booltrue37 : 
li, $s6, 1
boolfalse37 :
#
beqz $s6, iffalse37
#block
#assign
#intliteral
li $s5, 1
#
#varexpr
la, $s2, -12($fp)
#
la $t9, 0($s2)
lw $s3, 0($s2)
sw $s5, 0($t9)
#
#
b iftrue37
iffalse37 :
iftrue37 :
#
#
b iftrue36
iffalse36 :
#block
#if
#binop
#varexpr
la, $s6, -20($gp)
#
la $t9, 0($s6)
lw $s5, 0($s6)
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s3, 0($s6)
beq $s5, $s3, booltrue38
li, $s6, 0
b, boolfalse38
booltrue38 : 
li, $s6, 1
boolfalse38 :
#
beqz $s6, iffalse38
#block
#if
#binop
#varexpr
la, $s3, -36($gp)
#
la $t9, 0($s3)
lw $s5, 0($s3)
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s2, 0($s3)
beq $s5, $s2, booltrue39
li, $s3, 0
b, boolfalse39
booltrue39 : 
li, $s3, 1
boolfalse39 :
#
beqz $s3, iffalse39
#block
#assign
#intliteral
li $s2, 1
#
#varexpr
la, $s5, -12($fp)
#
la $t9, 0($s5)
lw $s7, 0($s5)
sw $s2, 0($t9)
#
#
b iftrue39
iffalse39 :
iftrue39 :
#
#
b iftrue38
iffalse38 :
#block
#if
#binop
#varexpr
la, $s3, -8($gp)
#
la $t9, 0($s3)
lw $s2, 0($s3)
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s7, 0($s3)
beq $s2, $s7, booltrue40
li, $s3, 0
b, boolfalse40
booltrue40 : 
li, $s3, 1
boolfalse40 :
#
beqz $s3, iffalse40
#block
#if
#binop
#varexpr
la, $s7, -12($gp)
#
la $t9, 0($s7)
lw $s2, 0($s7)
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s5, 0($s7)
beq $s2, $s5, booltrue41
li, $s7, 0
b, boolfalse41
booltrue41 : 
li, $s7, 1
boolfalse41 :
#
beqz $s7, iffalse41
#block
#assign
#intliteral
li $s5, 1
#
#varexpr
la, $s2, -12($fp)
#
la $t9, 0($s2)
lw $s1, 0($s2)
sw $s5, 0($t9)
#
#
b iftrue41
iffalse41 :
iftrue41 :
#
#
b iftrue40
iffalse40 :
iftrue40 :
#
#
iftrue38 :
#
#
iftrue36 :
#
#
b iftrue35
iffalse35 :
iftrue35 :
#
#if
#binop
#varexpr
la, $s4, -8($gp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s6, 0($s4)
beq $t8, $s6, booltrue42
li, $s4, 0
b, boolfalse42
booltrue42 : 
li, $s4, 1
boolfalse42 :
#
beqz $s4, iffalse42
#block
#if
#binop
#varexpr
la, $s6, -20($gp)
#
la $t9, 0($s6)
lw $t8, 0($s6)
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s3, 0($s6)
beq $t8, $s3, booltrue43
li, $s6, 0
b, boolfalse43
booltrue43 : 
li, $s6, 1
boolfalse43 :
#
beqz $s6, iffalse43
#block
#if
#binop
#varexpr
la, $s3, -32($gp)
#
la $t9, 0($s3)
lw $t8, 0($s3)
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s7, 0($s3)
beq $t8, $s7, booltrue44
li, $s3, 0
b, boolfalse44
booltrue44 : 
li, $s3, 1
boolfalse44 :
#
beqz $s3, iffalse44
#block
#assign
#intliteral
li $s7, 1
#
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
sw $s7, 0($t9)
#
#
b iftrue44
iffalse44 :
iftrue44 :
#
#
b iftrue43
iffalse43 :
iftrue43 :
#
#
b iftrue42
iffalse42 :
iftrue42 :
#
#if
#binop
#varexpr
la, $s4, -12($gp)
#
la $t9, 0($s4)
lw $s6, 0($s4)
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s3, 0($s4)
beq $s6, $s3, booltrue45
li, $s4, 0
b, boolfalse45
booltrue45 : 
li, $s4, 1
boolfalse45 :
#
beqz $s4, iffalse45
#block
#if
#binop
#varexpr
la, $s3, -24($gp)
#
la $t9, 0($s3)
lw $s6, 0($s3)
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s7, 0($s3)
beq $s6, $s7, booltrue46
li, $s3, 0
b, boolfalse46
booltrue46 : 
li, $s3, 1
boolfalse46 :
#
beqz $s3, iffalse46
#block
#if
#binop
#varexpr
la, $s7, -36($gp)
#
la $t9, 0($s7)
lw $s6, 0($s7)
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $s5, 0($s7)
beq $s6, $s5, booltrue47
li, $s7, 0
b, boolfalse47
booltrue47 : 
li, $s7, 1
boolfalse47 :
#
beqz $s7, iffalse47
#block
#assign
#intliteral
li $s5, 1
#
#varexpr
la, $s6, -12($fp)
#
la $t9, 0($s6)
lw $t8, 0($s6)
sw $s5, 0($t9)
#
#
b iftrue47
iffalse47 :
iftrue47 :
#
#
b iftrue46
iffalse46 :
#block
#if
#binop
#varexpr
la, $s7, -20($gp)
#
la $t9, 0($s7)
lw $s5, 0($s7)
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $t8, 0($s7)
beq $s5, $t8, booltrue48
li, $s7, 0
b, boolfalse48
booltrue48 : 
li, $s7, 1
boolfalse48 :
#
beqz $s7, iffalse48
#block
#if
#binop
#varexpr
la, $t8, -28($gp)
#
la $t9, 0($t8)
lw $s5, 0($t8)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
beq $s5, $s6, booltrue49
li, $t8, 0
b, boolfalse49
booltrue49 : 
li, $t8, 1
boolfalse49 :
#
beqz $t8, iffalse49
#block
#assign
#intliteral
li $s6, 1
#
#varexpr
la, $s5, -12($fp)
#
la $t9, 0($s5)
lw $s1, 0($s5)
sw $s6, 0($t9)
#
#
b iftrue49
iffalse49 :
iftrue49 :
#
#
b iftrue48
iffalse48 :
iftrue48 :
#
#
iftrue46 :
#
#
b iftrue45
iffalse45 :
iftrue45 :
#
#if
#binop
#varexpr
la, $s4, -16($gp)
#
la $t9, 0($s4)
lw $s3, 0($s4)
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s7, 0($s4)
beq $s3, $s7, booltrue50
li, $s4, 0
b, boolfalse50
booltrue50 : 
li, $s4, 1
boolfalse50 :
#
beqz $s4, iffalse50
#block
#if
#binop
#varexpr
la, $s7, -20($gp)
#
la $t9, 0($s7)
lw $s3, 0($s7)
#varexpr
la, $s7, -8($fp)
#
la $t9, 0($s7)
lw $t8, 0($s7)
beq $s3, $t8, booltrue51
li, $s7, 0
b, boolfalse51
booltrue51 : 
li, $s7, 1
boolfalse51 :
#
beqz $s7, iffalse51
#block
#if
#binop
#varexpr
la, $t8, -24($gp)
#
la $t9, 0($t8)
lw $s3, 0($t8)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
beq $s3, $s6, booltrue52
li, $t8, 0
b, boolfalse52
booltrue52 : 
li, $t8, 1
boolfalse52 :
#
beqz $t8, iffalse52
#block
#assign
#intliteral
li $s6, 1
#
#varexpr
la, $s3, -12($fp)
#
la $t9, 0($s3)
lw $s1, 0($s3)
sw $s6, 0($t9)
#
#
b iftrue52
iffalse52 :
iftrue52 :
#
#
b iftrue51
iffalse51 :
iftrue51 :
#
#
b iftrue50
iffalse50 :
iftrue50 :
#
#if
#binop
#varexpr
la, $s4, -28($gp)
#
la $t9, 0($s4)
lw $s7, 0($s4)
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
beq $s7, $t8, booltrue53
li, $s4, 0
b, boolfalse53
booltrue53 : 
li, $s4, 1
boolfalse53 :
#
beqz $s4, iffalse53
#block
#if
#binop
#varexpr
la, $t8, -32($gp)
#
la $t9, 0($t8)
lw $s7, 0($t8)
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
beq $s7, $s6, booltrue54
li, $t8, 0
b, boolfalse54
booltrue54 : 
li, $t8, 1
boolfalse54 :
#
beqz $t8, iffalse54
#block
#if
#binop
#varexpr
la, $s6, -36($gp)
#
la $t9, 0($s6)
lw $s7, 0($s6)
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s1, 0($s6)
beq $s7, $s1, booltrue55
li, $s6, 0
b, boolfalse55
booltrue55 : 
li, $s6, 1
boolfalse55 :
#
beqz $s6, iffalse55
#block
#assign
#intliteral
li $s1, 1
#
#varexpr
la, $s7, -12($fp)
#
la $t9, 0($s7)
lw $s3, 0($s7)
sw $s1, 0($t9)
#
#
b iftrue55
iffalse55 :
iftrue55 :
#
#
b iftrue54
iffalse54 :
iftrue54 :
#
#
b iftrue53
iffalse53 :
iftrue53 :
#
#return
#varexpr
la, $s4, -12($fp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
move $v0, $t8
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4
#
#fundecl

main:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
subu $sp, $sp, 36
#block
#vardecl
#
#vardecl
#
#vardecl
#
#vardecl
#
#assign
#charliteral
li $t8, ' '
#
#varexpr
la, $s4, -40($gp)
#
la $t9, 0($s4)
lw $s6, 0($s4)
sw $t8, 0($t9)
#
#assign
#intliteral
li $t8, 1
#
#varexpr
la, $s6, -8($fp)
#
la $t9, 0($s6)
lw $s4, 0($s6)
sw $t8, 0($t9)
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -24($fp)
sw $t4, -28($fp)
sw $t2, -32($fp)
sw $s2, -36($fp)
sw $t8, -40($fp)
sw $s4, -44($fp)
sw $t6, -48($fp)
sw $s7, -52($fp)
sw $s5, -56($fp)
sw $t1, -60($fp)
sw $s0, -64($fp)
sw $s6, -68($fp)
sw $t0, -72($fp)
sw $s1, -76($fp)
sw $s3, -80($fp)
sw $t7, -84($fp)
sw $t5, -88($fp)
sw $t9, -92($fp)
jal reset
lw $t9, -92($fp)
lw $t5, -88($fp)
lw $t7, -84($fp)
lw $s3, -80($fp)
lw $s1, -76($fp)
lw $t0, -72($fp)
lw $s6, -68($fp)
lw $s0, -64($fp)
lw $t1, -60($fp)
lw $s5, -56($fp)
lw $s7, -52($fp)
lw $t6, -48($fp)
lw $s4, -44($fp)
lw $t8, -40($fp)
lw $s2, -36($fp)
lw $t2, -32($fp)
lw $t4, -28($fp)
#
move $t8, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -96($fp)
sw $t4, -100($fp)
sw $t2, -104($fp)
sw $s2, -108($fp)
sw $t8, -112($fp)
sw $s4, -116($fp)
sw $t6, -120($fp)
sw $s7, -124($fp)
sw $s5, -128($fp)
sw $t1, -132($fp)
sw $s0, -136($fp)
sw $s6, -140($fp)
sw $t0, -144($fp)
sw $s1, -148($fp)
sw $s3, -152($fp)
sw $t7, -156($fp)
sw $t5, -160($fp)
sw $t9, -164($fp)
jal printGame
lw $t9, -164($fp)
lw $t5, -160($fp)
lw $t7, -156($fp)
lw $s3, -152($fp)
lw $s1, -148($fp)
lw $t0, -144($fp)
lw $s6, -140($fp)
lw $s0, -136($fp)
lw $t1, -132($fp)
lw $s5, -128($fp)
lw $s7, -124($fp)
lw $t6, -120($fp)
lw $s4, -116($fp)
lw $t8, -112($fp)
lw $s2, -108($fp)
lw $t2, -104($fp)
lw $t4, -100($fp)
#
move $t8, $v0
#
#assign
#intliteral
li $t8, 1
#
#varexpr
la, $s4, -12($fp)
#
la $t9, 0($s4)
lw $s6, 0($s4)
sw $t8, 0($t9)
#
#while
while1:
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s6, 0($t8)
beqz, $s6, whilefalse1
#block
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -168($fp)
sw $t4, -172($fp)
sw $t2, -176($fp)
sw $s2, -180($fp)
sw $t8, -184($fp)
sw $s4, -188($fp)
sw $t6, -192($fp)
sw $s7, -196($fp)
sw $s5, -200($fp)
sw $t1, -204($fp)
sw $s0, -208($fp)
sw $s6, -212($fp)
sw $t0, -216($fp)
sw $s1, -220($fp)
sw $s3, -224($fp)
sw $t7, -228($fp)
sw $t5, -232($fp)
sw $t9, -236($fp)
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal selectmove
lw $t9, -236($fp)
lw $t5, -232($fp)
lw $t7, -228($fp)
lw $s3, -224($fp)
lw $s1, -220($fp)
lw $t0, -216($fp)
lw $s6, -212($fp)
lw $s0, -208($fp)
lw $t1, -204($fp)
lw $s5, -200($fp)
lw $s7, -196($fp)
lw $t6, -192($fp)
lw $s4, -188($fp)
lw $t8, -184($fp)
lw $s2, -180($fp)
lw $t2, -176($fp)
lw $t4, -172($fp)
#
move $s4, $v0
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -240($fp)
sw $t4, -244($fp)
sw $t2, -248($fp)
sw $s2, -252($fp)
sw $t8, -256($fp)
sw $s4, -260($fp)
sw $t6, -264($fp)
sw $s7, -268($fp)
sw $s5, -272($fp)
sw $t1, -276($fp)
sw $s0, -280($fp)
sw $s6, -284($fp)
sw $t0, -288($fp)
sw $s1, -292($fp)
sw $s3, -296($fp)
sw $t7, -300($fp)
sw $t5, -304($fp)
sw $t9, -308($fp)
#varexpr
la, $s4, -12($fp)
#
la $t9, 0($s4)
lw $t8, 0($s4)
subu $sp, $sp, 4
sw $t8, 4($sp)
jal get_mark
lw $t9, -308($fp)
lw $t5, -304($fp)
lw $t7, -300($fp)
lw $s3, -296($fp)
lw $s1, -292($fp)
lw $t0, -288($fp)
lw $s6, -284($fp)
lw $s0, -280($fp)
lw $t1, -276($fp)
lw $s5, -272($fp)
lw $s7, -268($fp)
lw $t6, -264($fp)
lw $s4, -260($fp)
lw $t8, -256($fp)
lw $s2, -252($fp)
lw $t2, -248($fp)
lw $t4, -244($fp)
#
move $t8, $v0
#varexpr
la, $s4, -16($fp)
#
la $t9, 0($s4)
lw $s1, 0($s4)
sw $t8, 0($t9)
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -312($fp)
sw $t4, -316($fp)
sw $t2, -320($fp)
sw $s2, -324($fp)
sw $t8, -328($fp)
sw $s4, -332($fp)
sw $t6, -336($fp)
sw $s7, -340($fp)
sw $s5, -344($fp)
sw $t1, -348($fp)
sw $s0, -352($fp)
sw $s6, -356($fp)
sw $t0, -360($fp)
sw $s1, -364($fp)
sw $s3, -368($fp)
sw $t7, -372($fp)
sw $t5, -376($fp)
sw $t9, -380($fp)
jal printGame
lw $t9, -380($fp)
lw $t5, -376($fp)
lw $t7, -372($fp)
lw $s3, -368($fp)
lw $s1, -364($fp)
lw $t0, -360($fp)
lw $s6, -356($fp)
lw $s0, -352($fp)
lw $t1, -348($fp)
lw $s5, -344($fp)
lw $s7, -340($fp)
lw $t6, -336($fp)
lw $s4, -332($fp)
lw $t8, -328($fp)
lw $s2, -324($fp)
lw $t2, -320($fp)
lw $t4, -316($fp)
#
move $t8, $v0
#
#if
#funcallexpr
subu $sp, $sp, 72
sw $t3, -384($fp)
sw $t4, -388($fp)
sw $t2, -392($fp)
sw $s2, -396($fp)
sw $t8, -400($fp)
sw $s4, -404($fp)
sw $t6, -408($fp)
sw $s7, -412($fp)
sw $s5, -416($fp)
sw $t1, -420($fp)
sw $s0, -424($fp)
sw $s6, -428($fp)
sw $t0, -432($fp)
sw $s1, -436($fp)
sw $s3, -440($fp)
sw $t7, -444($fp)
sw $t5, -448($fp)
sw $t9, -452($fp)
#varexpr
la, $t8, -16($fp)
#
la $t9, 0($t8)
lw $s1, 0($t8)
subu $sp, $sp, 4
sw $s1, 4($sp)
jal won
lw $t9, -452($fp)
lw $t5, -448($fp)
lw $t7, -444($fp)
lw $s3, -440($fp)
lw $s1, -436($fp)
lw $t0, -432($fp)
lw $s6, -428($fp)
lw $s0, -424($fp)
lw $t1, -420($fp)
lw $s5, -416($fp)
lw $s7, -412($fp)
lw $t6, -408($fp)
lw $s4, -404($fp)
lw $t8, -400($fp)
lw $s2, -396($fp)
lw $t2, -392($fp)
lw $t4, -388($fp)
#
move $s1, $v0
beqz $s1, iffalse56
#block
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -456($fp)
sw $t4, -460($fp)
sw $t2, -464($fp)
sw $s2, -468($fp)
sw $t8, -472($fp)
sw $s4, -476($fp)
sw $t6, -480($fp)
sw $s7, -484($fp)
sw $s5, -488($fp)
sw $t1, -492($fp)
sw $s0, -496($fp)
sw $s6, -500($fp)
sw $t0, -504($fp)
sw $s1, -508($fp)
sw $s3, -512($fp)
sw $t7, -516($fp)
sw $t5, -520($fp)
sw $t9, -524($fp)
#varexpr
la, $t8, -12($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
subu $sp, $sp, 4
sw $s4, 4($sp)
jal printWinner
lw $t9, -524($fp)
lw $t5, -520($fp)
lw $t7, -516($fp)
lw $s3, -512($fp)
lw $s1, -508($fp)
lw $t0, -504($fp)
lw $s6, -500($fp)
lw $s0, -496($fp)
lw $t1, -492($fp)
lw $s5, -488($fp)
lw $s7, -484($fp)
lw $t6, -480($fp)
lw $s4, -476($fp)
lw $t8, -472($fp)
lw $s2, -468($fp)
lw $t2, -464($fp)
lw $t4, -460($fp)
#
move $s4, $v0
#
#assign
#intliteral
li $s4, 0
#
#varexpr
la, $t8, -8($fp)
#
la $t9, 0($t8)
lw $s3, 0($t8)
sw $s4, 0($t9)
#
#
b iftrue56
iffalse56 :
#if
#binop
#funcallexpr
subu $sp, $sp, 72
sw $t3, -456($fp)
sw $t4, -460($fp)
sw $t2, -464($fp)
sw $s2, -468($fp)
sw $t8, -472($fp)
sw $s4, -476($fp)
sw $t6, -480($fp)
sw $s7, -484($fp)
sw $s5, -488($fp)
sw $t1, -492($fp)
sw $s0, -496($fp)
sw $s6, -500($fp)
sw $t0, -504($fp)
sw $s1, -508($fp)
sw $s3, -512($fp)
sw $t7, -516($fp)
sw $t5, -520($fp)
sw $t9, -524($fp)
jal full
lw $t9, -524($fp)
lw $t5, -520($fp)
lw $t7, -516($fp)
lw $s3, -512($fp)
lw $s1, -508($fp)
lw $t0, -504($fp)
lw $s6, -500($fp)
lw $s0, -496($fp)
lw $t1, -492($fp)
lw $s5, -488($fp)
lw $s7, -484($fp)
lw $t6, -480($fp)
lw $s4, -476($fp)
lw $t8, -472($fp)
lw $s2, -468($fp)
lw $t2, -464($fp)
lw $t4, -460($fp)
#
move $s4, $v0
#intliteral
li $s3, 1
#
beq $s4, $s3, booltrue56
li, $t8, 0
b, boolfalse56
booltrue56 : 
li, $t8, 1
boolfalse56 :
#
beqz $t8, iffalse57
#block
#exprstmt
#funcallexpr
#strliteral
la $s3, str25
#
move $a0, $s3
jal print_s
#
move $s3, $v0
#
#assign
#intliteral
li $s3, 0
#
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s7, 0($s4)
sw $s3, 0($t9)
#
#
b iftrue57
iffalse57 :
#block
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -528($fp)
sw $t4, -532($fp)
sw $t2, -536($fp)
sw $s2, -540($fp)
sw $t8, -544($fp)
sw $s4, -548($fp)
sw $t6, -552($fp)
sw $s7, -556($fp)
sw $s5, -560($fp)
sw $t1, -564($fp)
sw $s0, -568($fp)
sw $s6, -572($fp)
sw $t0, -576($fp)
sw $s1, -580($fp)
sw $s3, -584($fp)
sw $t7, -588($fp)
sw $t5, -592($fp)
sw $t9, -596($fp)
#varexpr
la, $s3, -12($fp)
#
la $t9, 0($s3)
lw $s7, 0($s3)
subu $sp, $sp, 4
sw $s7, 4($sp)
jal switchPlayer
lw $t9, -596($fp)
lw $t5, -592($fp)
lw $t7, -588($fp)
lw $s3, -584($fp)
lw $s1, -580($fp)
lw $t0, -576($fp)
lw $s6, -572($fp)
lw $s0, -568($fp)
lw $t1, -564($fp)
lw $s5, -560($fp)
lw $s7, -556($fp)
lw $t6, -552($fp)
lw $s4, -548($fp)
lw $t8, -544($fp)
lw $s2, -540($fp)
lw $t2, -536($fp)
lw $t4, -532($fp)
#
move $s7, $v0
#varexpr
la, $s3, -12($fp)
#
la $t9, 0($s3)
lw $s4, 0($s3)
sw $s7, 0($t9)
#
#
iftrue57 :
#
iftrue56 :
#
#if
#binop
#varexpr
la, $s1, -8($fp)
#
la $t9, 0($s1)
lw $t8, 0($s1)
#intliteral
li $s1, 0
#
beq $t8, $s1, booltrue57
li, $s7, 0
b, boolfalse57
booltrue57 : 
li, $s7, 1
boolfalse57 :
#
beqz $s7, iffalse58
#block
#exprstmt
#funcallexpr
#strliteral
la $s1, str26
#
move $a0, $s1
jal print_s
#
move $s1, $v0
#
#assign
#funcallexpr
subu $sp, $sp, 72
sw $t3, -528($fp)
sw $t4, -532($fp)
sw $t2, -536($fp)
sw $s2, -540($fp)
sw $t8, -544($fp)
sw $s4, -548($fp)
sw $t6, -552($fp)
sw $s7, -556($fp)
sw $s5, -560($fp)
sw $t1, -564($fp)
sw $s0, -568($fp)
sw $s6, -572($fp)
sw $t0, -576($fp)
sw $s1, -580($fp)
sw $s3, -584($fp)
sw $t7, -588($fp)
sw $t5, -592($fp)
sw $t9, -596($fp)
jal read_c
lw $t9, -596($fp)
lw $t5, -592($fp)
lw $t7, -588($fp)
lw $s3, -584($fp)
lw $s1, -580($fp)
lw $t0, -576($fp)
lw $s6, -572($fp)
lw $s0, -568($fp)
lw $t1, -564($fp)
lw $s5, -560($fp)
lw $s7, -556($fp)
lw $t6, -552($fp)
lw $s4, -548($fp)
lw $t8, -544($fp)
lw $s2, -540($fp)
lw $t2, -536($fp)
lw $t4, -532($fp)
#
move $s1, $v0
#varexpr
la, $t8, -20($fp)
#
la $t9, 0($t8)
lw $s4, 0($t8)
sw $s1, 0($t9)
#
#if
#binop
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s4, 0($s1)
#charliteral
li $s1, 'y'
#
beq $s4, $s1, booltrue58
li, $t8, 0
b, boolfalse58
booltrue58 : 
li, $t8, 1
boolfalse58 :
#
beqz $t8, iffalse59
#block
#assign
#intliteral
li $s1, 1
#
#varexpr
la, $s4, -8($fp)
#
la $t9, 0($s4)
lw $s3, 0($s4)
sw $s1, 0($t9)
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -600($fp)
sw $t4, -604($fp)
sw $t2, -608($fp)
sw $s2, -612($fp)
sw $t8, -616($fp)
sw $s4, -620($fp)
sw $t6, -624($fp)
sw $s7, -628($fp)
sw $s5, -632($fp)
sw $t1, -636($fp)
sw $s0, -640($fp)
sw $s6, -644($fp)
sw $t0, -648($fp)
sw $s1, -652($fp)
sw $s3, -656($fp)
sw $t7, -660($fp)
sw $t5, -664($fp)
sw $t9, -668($fp)
jal read_c
lw $t9, -668($fp)
lw $t5, -664($fp)
lw $t7, -660($fp)
lw $s3, -656($fp)
lw $s1, -652($fp)
lw $t0, -648($fp)
lw $s6, -644($fp)
lw $s0, -640($fp)
lw $t1, -636($fp)
lw $s5, -632($fp)
lw $s7, -628($fp)
lw $t6, -624($fp)
lw $s4, -620($fp)
lw $t8, -616($fp)
lw $s2, -612($fp)
lw $t2, -608($fp)
lw $t4, -604($fp)
#
move $s1, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -672($fp)
sw $t4, -676($fp)
sw $t2, -680($fp)
sw $s2, -684($fp)
sw $t8, -688($fp)
sw $s4, -692($fp)
sw $t6, -696($fp)
sw $s7, -700($fp)
sw $s5, -704($fp)
sw $t1, -708($fp)
sw $s0, -712($fp)
sw $s6, -716($fp)
sw $t0, -720($fp)
sw $s1, -724($fp)
sw $s3, -728($fp)
sw $t7, -732($fp)
sw $t5, -736($fp)
sw $t9, -740($fp)
jal reset
lw $t9, -740($fp)
lw $t5, -736($fp)
lw $t7, -732($fp)
lw $s3, -728($fp)
lw $s1, -724($fp)
lw $t0, -720($fp)
lw $s6, -716($fp)
lw $s0, -712($fp)
lw $t1, -708($fp)
lw $s5, -704($fp)
lw $s7, -700($fp)
lw $t6, -696($fp)
lw $s4, -692($fp)
lw $t8, -688($fp)
lw $s2, -684($fp)
lw $t2, -680($fp)
lw $t4, -676($fp)
#
move $s1, $v0
#
#
b iftrue59
iffalse59 :
#block
#if
#binop
#varexpr
la, $s1, -20($fp)
#
la $t9, 0($s1)
lw $s3, 0($s1)
#charliteral
li $s1, 'Y'
#
beq $s3, $s1, booltrue59
li, $s4, 0
b, boolfalse59
booltrue59 : 
li, $s4, 1
boolfalse59 :
#
beqz $s4, iffalse60
#block
#assign
#intliteral
li $s1, 1
#
#varexpr
la, $s3, -8($fp)
#
la $t9, 0($s3)
lw $s5, 0($s3)
sw $s1, 0($t9)
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -600($fp)
sw $t4, -604($fp)
sw $t2, -608($fp)
sw $s2, -612($fp)
sw $t8, -616($fp)
sw $s4, -620($fp)
sw $t6, -624($fp)
sw $s7, -628($fp)
sw $s5, -632($fp)
sw $t1, -636($fp)
sw $s0, -640($fp)
sw $s6, -644($fp)
sw $t0, -648($fp)
sw $s1, -652($fp)
sw $s3, -656($fp)
sw $t7, -660($fp)
sw $t5, -664($fp)
sw $t9, -668($fp)
jal read_c
lw $t9, -668($fp)
lw $t5, -664($fp)
lw $t7, -660($fp)
lw $s3, -656($fp)
lw $s1, -652($fp)
lw $t0, -648($fp)
lw $s6, -644($fp)
lw $s0, -640($fp)
lw $t1, -636($fp)
lw $s5, -632($fp)
lw $s7, -628($fp)
lw $t6, -624($fp)
lw $s4, -620($fp)
lw $t8, -616($fp)
lw $s2, -612($fp)
lw $t2, -608($fp)
lw $t4, -604($fp)
#
move $s1, $v0
#
#exprstmt
#funcallexpr
subu $sp, $sp, 72
sw $t3, -672($fp)
sw $t4, -676($fp)
sw $t2, -680($fp)
sw $s2, -684($fp)
sw $t8, -688($fp)
sw $s4, -692($fp)
sw $t6, -696($fp)
sw $s7, -700($fp)
sw $s5, -704($fp)
sw $t1, -708($fp)
sw $s0, -712($fp)
sw $s6, -716($fp)
sw $t0, -720($fp)
sw $s1, -724($fp)
sw $s3, -728($fp)
sw $t7, -732($fp)
sw $t5, -736($fp)
sw $t9, -740($fp)
jal reset
lw $t9, -740($fp)
lw $t5, -736($fp)
lw $t7, -732($fp)
lw $s3, -728($fp)
lw $s1, -724($fp)
lw $t0, -720($fp)
lw $s6, -716($fp)
lw $s0, -712($fp)
lw $t1, -708($fp)
lw $s5, -704($fp)
lw $s7, -700($fp)
lw $t6, -696($fp)
lw $s4, -692($fp)
lw $t8, -688($fp)
lw $s2, -684($fp)
lw $t2, -680($fp)
lw $t4, -676($fp)
#
move $s1, $v0
#
#
b iftrue60
iffalse60 :
iftrue60 :
#
#
iftrue59 :
#
#
b iftrue58
iffalse58 :
iftrue58 :
#
#
b while1
whilefalse1:
#
#
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 0
#

#Permenent functions

print_s:
li $v0, 4
syscall
jr $ra

print_i:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
lw $a0, 4($fp)
li $v0, 1
syscall
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4

print_c:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
lw $a0, 4($fp)
li $v0, 11
syscall
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
add $sp, $sp, 4

read_c:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
li $v0, 12
syscall
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra

read_i:
subu $sp, $sp, 8
sw $fp, 8($sp)
sw $ra, 4($sp)
addiu $fp, $sp, 8
li $v0, 5
syscall
move $sp, $fp
lw $ra, -4($fp)
lw $fp, 0($fp)
jr $ra
#
