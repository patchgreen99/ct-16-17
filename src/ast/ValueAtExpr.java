package ast;

/**
 * Created by patrick.green on 22/10/2016.
 */
public class ValueAtExpr extends Expr{
    public final Expr expr;

    public String struct2return;

    public ValueAtExpr(Expr expr){
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {return v.visitValueAtExpr(this);}
}

